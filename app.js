"use strict";
//  Default server port
const PORT = 8080;
const DBPATH = 'mongodb://localhost:4586/dotattoo';
const CONNECT_OPTION = {
    user: 'api_dotattoo',
    pass: 'dotattoo0119'
};

const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const webEngine =  require('ejs-mate');
const favicon = require('serve-favicon');

const app = express();
const web = require('./routes/web');
const member = require('./routes/member');
const post = require('./routes/post');
const timeline = require('./routes/timeline');
const search = require('./routes/search');
const pickbook = require('./routes/pickbook');
const message = require('./routes/message');
const tattoo = require('./routes/tattoo');
const push = require('./routes/push').router;
const share = require('./routes/share');
const notification = require('./routes/notification');

//  Set up server
app.use(express.static('images'));
app.use(express.static('res'));

//  apidoc static routes
app.use(express.static(__dirname + '/apidoc/api.html'));
app.use('/css', express.static(__dirname + '/apidoc/css'));
app.use('/img', express.static(__dirname + '/apidoc/img'));
app.use('/vendor', express.static(__dirname + '/apidoc/vendor'));
app.use('/locales', express.static(__dirname + '/apidoc/locales'));
app.use('/utils', express.static(__dirname + '/apidoc/utils'));
app.use('/main.js', express.static(__dirname + '/apidoc/main.js'));
app.use('/api_data.js', express.static(__dirname + '/apidoc/api_data.js'));
app.use('/api_project.js', express.static(__dirname + '/apidoc/api_project.js'));

app.use(favicon(__dirname + '/res/image/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(logger('dev'));
//  set viewengine
app.engine('ejs', webEngine);
app.set('view engine', 'ejs');
app.set('views', __dirname + '/res/html');
app.use(checkIP);
app.use(allowCrossDomain);

//  route request
app.use('/', web);
app.use('/', member);
app.use('/', post);
app.use('/', timeline);
app.use('/', search);
app.use('/', pickbook);
app.use('/', message);
app.use('/', tattoo);
app.use('/', push);
app.use('/', share);
app.use('/', notification);
app.use('/', require('./routes/banner'));

//  Show api document page
app.get('/api', function(req, res) {
    res.sendFile(path.join(__dirname, 'apidoc/api.html'));
});

let reporter = require('./util/error');
let errorModel = require('./util/db-schema').Error;

run();

//  Run server with db connection
function run() {
    mongoose.connect(DBPATH, CONNECT_OPTION);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'db connection error while starting server..'));
    db.once('open', function() {
        app.listen(process.env.PORT || PORT, function() {
            console.log('Server is started at port ' + process.env.PORT);
        })
    });
}

//  Run server without db connection
function testRun(port) {
    app.listen(port, function() {
        console.log('Server is started at port ' + port);
    });
}

//CORS middleware
function allowCrossDomain(req, res, next) {
    var ip = req.query.ip;
    res.header('Access-Control-Allow-Origin', 'http://' + ip);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

function checkIP(req, res, next) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    req.userIp = ip;
    console.log('requested ip is ' + req.userIp);
    next();
}
