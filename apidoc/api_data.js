define({ "api": [
  {
    "type": "POST",
    "url": "/console/banner",
    "title": "add banner",
    "name": "add_banner_API",
    "group": "console",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>console user's access_token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "banner_img",
            "description": "<p>banner image url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "detail_img",
            "description": "<p>banner detail image</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>banner description</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/console/banner"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/web.js",
    "groupTitle": "console"
  },
  {
    "type": "DELETE",
    "url": "/console/banner",
    "title": "delete banner",
    "name": "delete_banner_API",
    "group": "console",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>console user's access_token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "banner_id",
            "description": "<p>what you want to delete.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/console/banner"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/web.js",
    "groupTitle": "console"
  },
  {
    "type": "POST",
    "url": "/login",
    "title": "login API for facebook and kakao. If user is not exist, then create new user and login.",
    "group": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"facebook\"",
              "\"kakao\""
            ],
            "optional": false,
            "field": "type",
            "description": "<p>login type for users</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>facebook/kakao access_token to get basic user information</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>It will be 'OK'</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>our service's user_id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>our service's access_token</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/member.js",
    "groupTitle": "login",
    "name": "PostLogin"
  },
  {
    "type": "DELETE",
    "url": "/post/:post_id/comment_id",
    "title": "delete a comment on post",
    "name": "deleteComment",
    "description": "<p>delete a comment on post by comment_id.</p>",
    "group": "post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/post/:post_id/comment"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/post.js",
    "groupTitle": "post"
  },
  {
    "type": "GET",
    "url": "/post/:post_id",
    "title": "get a post",
    "name": "getPost",
    "description": "<p>get a post by given post_id</p>",
    "group": "post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/post/:post_id"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/post.js",
    "groupTitle": "post"
  },
  {
    "type": "GET",
    "url": "/post/:post_id/comment",
    "title": "",
    "name": "get_comment_on_post",
    "description": "<p>get comments on a specific post by post_id</p>",
    "group": "post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "messageSchema",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/post/:post_id/comment"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/post.js",
    "groupTitle": "post"
  },
  {
    "type": "POST",
    "url": "/post/:id/comment",
    "title": "",
    "name": "uploadComment",
    "description": "<p>upload a comment on post.</p>",
    "group": "post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/post/:post_id/comment"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/post.js",
    "groupTitle": "post"
  },
  {
    "type": "POST",
    "url": "/post",
    "title": "upload a post",
    "name": "uploadPost",
    "description": "<p>upload a tattoo post using multipart upload.</p>",
    "group": "post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "part",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tattooist",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/post"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/post.js",
    "groupTitle": "post"
  },
  {
    "type": "POST",
    "url": "/push/console_send",
    "title": "send push message from manage console",
    "name": "consolePushAPI",
    "group": "push",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>console access_token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "push_message",
            "description": "<p>what you want to show your users.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "img_link",
            "description": "<p>popup image url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_ids",
            "description": "<p>user_id array that you want to send push.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/push/console_send"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/push.js",
    "groupTitle": "push"
  },
  {
    "type": "DELETE",
    "url": "/push/register",
    "title": "delete gcm registration_id",
    "name": "deletePushToken",
    "description": "<p>delete GCM push registration_id from database.</p>",
    "group": "push",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registration_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/push/register"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/push.js",
    "groupTitle": "push"
  },
  {
    "type": "POST",
    "url": "/push/register",
    "title": "save gcm registration_id",
    "name": "uploadRegistrationToken",
    "group": "push",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registration_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/push/register"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/push.js",
    "groupTitle": "push"
  },
  {
    "type": "GET",
    "url": "/search",
    "title": "Post & User Search API",
    "group": "search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"post\"",
              "\"user\""
            ],
            "optional": false,
            "field": "search_type",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/search"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/search.js",
    "groupTitle": "search",
    "name": "GetSearch"
  },
  {
    "type": "GET",
    "url": "/tattoo/reservation",
    "title": "get tattoo reservation",
    "name": "getReservation",
    "description": "<p>get one tattoo reservation by given reservation id.</p>",
    "group": "tattoo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reservation_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/tattoo/reservation"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/tattoo.js",
    "groupTitle": "tattoo"
  },
  {
    "type": "GET",
    "url": "/tattoo/reservation/list",
    "title": "show reservation list",
    "name": "getReservationList",
    "description": "<p>get tattoo reservation list related by given access_token</p>",
    "group": "tattoo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/tattoo/reservation/list"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/tattoo.js",
    "groupTitle": "tattoo"
  },
  {
    "type": "POST",
    "url": "/console/tattooist/state_change",
    "title": "change tattooist state",
    "name": "changeTattooistState",
    "group": "tattooist",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>web console user's access_token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"contact\"",
              "\"consult\""
            ],
            "optional": false,
            "field": "type",
            "description": "<p>change state type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "target_user_id",
            "description": "<p>user_id that you want to change.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/console/tattooist/state_change"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/web.js",
    "groupTitle": "tattooist"
  },
  {
    "type": "POST",
    "url": "/tattoo/tattooist/register",
    "title": "turn user to tattooist.",
    "name": "registerTattooist",
    "group": "tattoo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "target_user_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "real_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "place",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/tattoo/tattooist/register"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/tattoo.js",
    "groupTitle": "tattoo"
  },
  {
    "type": "POST",
    "url": "/tattoo/reservation",
    "title": "upload tattoo reservation",
    "name": "reservateTattoo",
    "description": "<p>upload tattoo reservation using received request from user.</p>",
    "group": "tattoo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "request_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone_num",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_name",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/tattoo/reservation"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/tattoo.js",
    "groupTitle": "tattoo"
  },
  {
    "type": "",
    "url": "/tattoo/tattooist/request",
    "title": "request to turn user to tattooist.",
    "name": "tattooist_request_api",
    "group": "tattoo",
    "description": "<p>request to turn user to a tattooist account</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "real_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "place",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "introduction",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/tattoo/tattooist/request"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/tattoo.js",
    "groupTitle": "tattoo"
  },
  {
    "type": "GET",
    "url": "/timeline",
    "title": "get DOTATTOO timeline",
    "name": "getTimeline",
    "group": "timeline",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>if it is provided, timeline information with liked information will be served.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/timeline"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "JSONArray",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/timeline.js",
    "groupTitle": "timeline"
  },
  {
    "type": "GET",
    "url": "/users/top",
    "title": "get top ranked 20 users",
    "group": "user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>It'll be 'OK'</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/member.js",
    "groupTitle": "user",
    "name": "GetUsersTop"
  },
  {
    "type": "GET",
    "url": "/user/:user_id",
    "title": "get user profile information",
    "name": "getUserProfile",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_oid",
            "description": "<p>want to get user's id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>It will be 'OK'</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/member.js",
    "groupTitle": "user"
  },
  {
    "type": "POST",
    "url": "/user/:user_id/update",
    "title": "Update user information except profile image",
    "name": "updateUserInformation",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "place",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>It'll be ok.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/member.js",
    "groupTitle": "user"
  }
] });
