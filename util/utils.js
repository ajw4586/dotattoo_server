'use strict';
module.exports = this;
//  date must has following format.
//  yyyy-MM-dd
//  ex) 2016-01-01
module.exports.parseDate = function(date) {
    let parts = date.split('-');
    return new Date(parts[0], parts[1]-1, parts[2]);
}
