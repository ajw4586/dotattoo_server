"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const errorReporter = require('./error');

const memberSchema = new Schema({
    name: String,
    gender: String,
    profile_img: String,
    phone: String,
    email: String,
    type: { type: String, default: 'user' },
    access_token: String,
    social: {
        facebook: {
            id: Number,
            token: String
        },
        kakao: {
            id: Number,
            token_type: String,
            token: String,
            refresh_token: String,
            expires_in: String,
            scope: String
        }
    },
    tattooist: {
        name: String,
        place: String,
        comment: String,
        address: String,
        introduction: String,
        bank_account: {
            bank: String,
            account_number: String,
            owner: String
        },
        status: {
            show_contact: { type: Boolean, default: false },
            show_consult: { type: Boolean, default: false }
        }
    }
});

memberSchema.statics.findUserById = function(user_id, callback) {
    if(user_id == undefined || user_id === '') {
        callback({
            status: 'error',
            message: 'user_id is not set.'
        });
        return;
    }
    this.findOne({'_id': user_id}, function(err, user) {
        if(!err && user) {
            callback({
                status: 'OK',
                user: user
            });
        } else {
            callback({
                status: 'error'
            });
            errorReporter.save(undefined, 'Failed to find user by id.', err);
        }
    })
}

memberSchema.statics.findUserName = function(access_token, callback) {
    return this.findOne({ 'access_token': access_token }, 'name', callback);
};

memberSchema.statics.getUserId = function(access_token, callback) {
    return this.findOne({ 'access_token': access_token }, '_id', callback);
};

memberSchema.statics.inspectAccessToken = function(access_token, callback) {
    if(access_token == undefined) {
        callback({
            status: 'error',
            message: 'access_token is undefined.'
        });
        return;
    }
    this.findOne({ 'access_token': access_token }, (err, user) => {
        if(!err && user) {
            callback({
                status: 'OK',
                message: 'Access Token is valid.',
                user_id: user._id
            });
        } else {
            callback({
                status: 'error',
                message: 'Access Token is not valid.',
                user_id: ''
            });
        }
    });
};

const postSchema = new Schema({
    title: String,
    comment: String,
    part: String,
    tattooist: String,
    photo_url: String,
    tag: [String],
    reaction: {
        like: [{
            type: Schema.Types.ObjectId,
            ref: 'Member'
        }],
        comment: [{
            comment: String,
            commentBy: {
                type: Schema.Types.ObjectId,
                ref: 'Member'
            },
            comment_time: {
                type: Date,
                default: Date.now
            }
    }],
        share: [Schema.Types.ObjectId]
    },
    info: {
        user_id: {
            type: Schema.Types.ObjectId,
            ref: 'Member'
        },
        post_time: Date,
        popularity: {
            type: Number,
            default: 0
        }
    }
});

/**
    Update like target post.
    @return status (OK/ERROR)
*/
postSchema.statics.like = function(post_id, user_id, callback) {
    this.findOne({ '_id': post_id })
    .where('reaction.like').ne(user_id)
    .select('reaction info')
    .exec(function(err, post) {
        if(err) {
            //  error handling
            return callback({
                status: 'error',
                message: 'db error.'
            });
        }
        //  already liked this post.
        if(!post) {
            return callback({
                status: 'error',
                message: 'Cannot like post.'
            });
        } else {
            //  like perform
            post.reaction.like.push(user_id);
            post.updatePopularity();
            post.save(function(err) {
                if(err) {
                    console.error.bind(console, 'A database exception while processing like on post.');
                    return callback({
                        status: 'error'
                    });
                }
                //  save notification to uploader.
                let noti = new module.exports.Notification({
                    user_id: user_id,
                    target_user_id: post.info.user_id,
                    path: post_id,
                    action: 'like',
                    time: Date.now()
                });
                noti.save(function(err) {
                    if(!err) {
                    } else {
                        console.log('An error occured while saving notification.');
                    }
                });
                callback({
                    status: 'OK'
                });
            });
        }
    });
};

postSchema.statics.unlike = function(post_id, user_id, callback) {
    this.findOne({ '_id': post_id, 'reaction.like': { '$in': [user_id] }})
    .select('reaction info.popularity')
    .exec(function(err, post) {
        if(err) {
            //  error handling
            return callback({
                status: 'error',
                message: 'db error.'
            });
        }
        //  already liked this post.
        if(!post) {
            return callback({
                status: 'error',
                message: 'Cannot unlike post.'
            });
        } else {
            //  like perform
            post.reaction.like.pull(user_id);
            post.updatePopularity();
            post.save(function(err) {
                if(err) {
                    console.error.bind(console, 'A database exception while processing like on post.');
                    return callback({
                        status: 'error',
                        message: 'db error.'
                    });
                }
                //  if notification is exist,
                //  remove the notification

                callback({
                    status: 'OK'
                });
            });
        }
    });
};


postSchema.statics.comment = function(post_id, user_id, comment, callback) {
    this.findOne({ '_id': post_id })
    .select('reaction info')
    .populate('info.user_id')
    .exec(function(err, post) {
        post.reaction.comment.push({
            comment: comment,
            commentBy: user_id,
            comment_time: Date.now()
        });
        post.updatePopularity();
        post.save(function(err) {
            if(err) {
                console.error.bind(console, 'A database exception while processing comment on post.');
                return callback({
                    status: 'error'
                });
            }
            //  add notification
            new module.exports.Notification({
                user_id: user_id,
                target_user_id: post.info.user_id,
                path: post._id,
                action: 'comment',
                time: Date.now()
            }).save(function(err) {
                if(err) {
                    console.log('An error while saving notification data.');
                }
            });
            //  To send gcm push
            module.exports.Member.findOne({ '_id': user_id })
                .exec(function(err, usr) {
                    if(!err && usr) {
                        require('../routes/push').push(usr, post.info.user_id, comment, 'comment');
                        callback({
                            status: 'OK'
                        });
                    } else {
                        //  GCM error
                        callback({
                            status: 'OK',
                            message: 'GCM failed.'
                        });
                    }
                });
        });
    });
};

postSchema.methods.updatePopularity = function() {
    this.info.popularity = this.reaction.like.length
    + this.reaction.comment.length
    + this.reaction.share.length;
    return this;
};

postSchema.statics.findUserPost = (user_id, callback) => {
    this.find({ 'info.user_id': user_id }, (err, posts) => {
        if(!err && posts) {
            callback({
                status: 'OK',
                posts: posts
            });
        } else {
            callback({
                status: 'error'
            });
            errorReporter(undefined, 'Failed to get posts uploaded by user', err);
        }
    });
};

const errorSchema = new Schema({
    time: Date,
    error: String,
    message: String,
    ip: String
});

errorSchema.statics.save = (ip, error, message) => {
    const Error = mongoose.model('Error');
    new Error({
        time: Date.now(),
        error: error,
        message: message,
        ip: ip
    }).save((_err) => {
        if(!_err) {
            console.log('An error reported into Error DB.');
        }
    });
}

const messageSchema = new Schema({
    from: Schema.Types.ObjectId,
    to: Schema.Types.ObjectId,
    type: String,
    message: String,
    time: {
        type: Date,
        default: Date.now
    },
    app_link: {
        link: String,
        type: String,
    },
    request: {
        photo: String,
        name: String,
        date: {
            type: Date,
            default: Date.now
        },
        price: Number
    }
});

/**
    save text type message.
*/
messageSchema.statics.textMessage = (user_id, receive_user_id, message, callback) => {
    const MESSAGE_TYPE_TEXT = 'text';
    const Message = mongoose.model('Message');
    let msg = new Message({
        from: user_id,
        to: receive_user_id,
        type: MESSAGE_TYPE_TEXT,
        message: message,
        time: Date.now()
    });
    msg.save(function(err) {
        if(!err) {
            //  Success
            callback({
                status: 'OK',
                time: msg.time
            });
        } else {
            //  Error response
            callback({
                status: 'error',
                message: 'db error'
            });
        }
    });
};

messageSchema.statics.appLinkMessage = (user_id, receive_user_id, link, callback) => {
    const MESSAGE_TYPE_APP_LINK = "app_link";
    const APP_LINK_POST_ID = 'post_id';
    const Message = mongoose.model('Message');
    new Message({
        from: user_id,
        to: receive_user_id,
        type: MESSAGE_TYPE_APP_LINK,
        time: Date.now(),
        app_link: {
            link: link,
            type: APP_LINK_POST_ID
        }
    }).save((err, msg) => {
        if(err) {
            callback({
                status: 'error',
                message: 'internal error'
            });
            console.error(err);
        }
        callback({
            status: 'OK',
            time: msg.time
        });
    });
};

/**
    save photo type message.
*/
messageSchema.statics.photoMessage = (user_id, receive_user_id, photo_url, callback) => {
    const MESSAGE_TYPE_PHOTO = 'photo';
    const Message = mongoose.model('Message');
    let msg = new Message({
        from: user_id,
        to: receive_user_id,
        type: MESSAGE_TYPE_PHOTO,
        message: photo_url,
        time: Date.now()
    });
    msg.save((err, msg) => {
        if(!err) {
            //  Success
            callback({
                status: 'OK',
                time: msg.time
            });
        } else {
            //  Error response
            callback({
                status: 'error',
                message: 'db error.'
            });
        }
    });
};

messageSchema.statics.requestMessage = (user_id, receive_user_id, request, callback) => {
    const MESSAGE_TYPE_REQUEST = 'request';
    const Message = mongoose.model('Message');
    let msg = new Message({
        from: user_id,
        to: receive_user_id,
        type: MESSAGE_TYPE_REQUEST,
        message: request._id,
        time: Date.now(),
        request: {
            photo: request.info.photo,
            name: request.info.tattoo_name,
            date: request.info.date,
            price: request.info.price
        }
    });
    msg.save((err) => {
        if(!err) {
            callback({
                status: 'OK',
                time: msg.time
            });
        } else {
            callback({
                status: 'error',
                message: 'db error.'
            });
        }
    });
};

const pickbookSchema = new Schema({
    name: String,
    create_by: Schema.Types.ObjectId,
    posts: [Schema.Types.ObjectId]
});

/**
    create pickbook
*/
pickbookSchema.statics.create = function(user_id, name, callback) {
    let pickbook = new Pickbook({
        name: name,
        create_by: user_id
    });
    pickbook.save(function(err) {
        if(!err) {
            //  Success response
        } else {
            //  Error response
        }
    });
};

pickbookSchema.statics.savePost = function(pickbook_id, post_id, callback) {
    this.findOne({ '_id': pickbook_id })
    .exec(function(err, book) {
        if(!err && book) {
            //  Perform save
        } else {
            callback({
                status: 'error',
                message: 'Cannot find any pickbook.'
            });
        }
    });
};

const pushTokenSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'Member'
    },
    registration_ids: [String]
});

//  add registration_id to user
//  if registration_id is already registered,
//  add a new token anyway.
pushTokenSchema.statics.addId = function(registration_id, user_id, callback) {
    const PushToken = mongoose.model('PushToken');
    PushToken.findOne({ user_id: user_id })
    .exec(function(err, results) {
        if(err) {
            callback({
                status: 'error',
                message: 'internal db error'
            });
        } else if(results) {
            //  check if the result contains what we want to add.
            //  if result already contains, return success
            let hasSameValue = false;
            results.registration_ids.forEach(function(token) {
                if(token == registration_id) {
                    hasSameValue = true;
                }
            });
            //  add if the result doesn't contain the value.
            if(!hasSameValue) {
                results.registration_ids.push(registration_id);
                results.save(function(err) {
                    if(!err) {
                        callback({
                            status: 'OK'
                        });
                    } else {
                        //  error response
                        callback({
                            status: 'error',
                            message: 'internal db error.'
                        });
                    }
                })
            } else {
                //  already registered push token id.
                callback({
                    status: 'OK'
                });
            }
        } else {
            //  create new document
            let push = new PushToken({
                user_id: user_id,
                registration_ids: [registration_id]
            });
            push.save(function(err) {
                if(!err) {
                    callback({
                        status: 'OK'
                    });
                } else {
                    //  error response
                    callback({
                        status: 'error',
                        message: 'internal db error.'
                    });
                }
            });
        }
    });
};
pushTokenSchema.statics.removeId = function(registration_id, user_id, callback) {
    const PushToken = mongoose.model('PushToken');
    PushToken.find({
        user_id: user_id,
        registration_ids: registration_id
    })
    .exec(function(err, result) {
        if(!err && result) {
            result.remove().exec();
            callback({
                status: 'OK'
            });
        } else {
            callback({
                status: 'error',
                message: 'No push token was found.'
            });
        }
    });
};

const requestSchema = new Schema({
    tattooist_id: {
        type: Schema.Types.ObjectId,
        ref: 'Member'
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'Member'
    },
    info: {
        photo: String,
        tattoo_name: String,
        genre: String,
        place: String,
        part: String,
        date: Date,
        term: Number,
        price: Number,
        reservation_price: Number
    },
    time: {
        type: Date,
        default: Date.now
    }
});

const reservationSchema = new Schema({
    request: {
        type: Schema.Types.ObjectId,
        ref: 'Request'
    },
    purchase_user_info: {
        birthday: String,
        phone_num: String,
        user_name: String
    },
    reservation_time: {
        type: Date,
        default: Date.now
    }
});

const notificationSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'Member'
    },
    target_user_id: {
        type: Schema.Types.ObjectId,
        ref: 'Member'
    },
    path: {
        type: Schema.Types.ObjectId
    },
    action: String,
    time: {
        type: Date,
        default: Date.now
    }
});

notificationSchema.statics.queryAllPostsNoti = function(user_id, callback) {
    Post.find({ 'info.user_id': user_id })
        .select('_id')
        .lean()
        .exec(function(err, post_ids) {
            if(!err && post_ids) {
                let ids = [];
                post_ids.forEach(function(post_id) {
                    ids.push({ path: id });
                });
                this.find()
                    .or(ids)
                    .exec(function(err, noti) {
                        if(!err && noti) {
                            callback({
                                status: 'OK',
                                data: noti
                            });
                        } else {
                            callback({
                                status: 'error',
                                message: 'internal db error.'
                            });
                        }
                    });
            } else {
                //  Not yey posted.
                callback({
                    status: 'error',
                    message: 'No post found.'
                })
            }
        });
};

notificationSchema.saveNotification = function(user_id, target_user_id, path, action, callback) {

};

const consoleMemberSchema = new Schema({
    id: String,
    pw: String,
    access_token: String,
    signup_date: {
        type: Date,
        default: Date.now
    },
    last_login_time: Date
});

consoleMemberSchema.statics.inspectConsoleAccessToken = (access_token, callback) => {
    if(access_token == undefined) {
        callback({
            status: 'error',
            message: 'access_token is undefined.'
        });
        return;
    }
    let ConsoleMember = mongoose.model('ConsoleMember');
    ConsoleMember.findOne({ access_token: access_token })
        .exec((err, user) => {
            if(!err && user) {
                callback({
                    status: 'OK',
                    message: 'Access Token is valid.',
                    user_id: user._id
                });
            } else {
                callback({
                    status: 'error',
                    message: 'Access Token is not valid.'
                });
            }

        });
};


const tattooistRequestSchema = new Schema({
    request_user_id: { type: Schema.Types.ObjectId, ref: 'Member' },
    real_name: String,
    place: String,
    address: String,
    email: String,
    phone: String,
    introduction: String,
    bank: {
        account: String,
        name: String,
        owner: String
    },
    requested_time: { type: Date, default: Date.now },
    //  It must be 'requested' and 'done'.
    status: {
        type: String,
        default: 'requested'
    }
});

tattooistRequestSchema.statics.done = (request_id, callback) => {
    if(!request_id) {
        if(callback) {
            callback({
                status: 'error',
                message: 'request_id is undefined.'
            });
        }
        return;
    }
    let TattooistRequest = mongoose.model('TattooistRequest');
    TattooistRequest.findOne({ _id: request_id })
        .exec((err, request) => {
            if(!err && request) {
                request.status = 'done';
                request.save((err) => {
                    if(!err) {
                        //  success to save
                        if(callback) {
                            callback({
                                status: 'OK'
                            })
                        }
                    } else {
                        //  handle error
                        if(!err) {
                            if(callback) {
                                callback({
                                    status: 'error',
                                    message: 'Failed to change request to done.'
                                });
                            }
                        }
                    }
                });
            } else {
                if(callback) {
                    callback({
                        status: 'error',
                        message: 'Failed to find request..'
                    });
                }
            }
        });
};

const bannerSchema = new Schema({
    img_url: String,
    web_link: String,
    description: String,
    upload_time: { type: Date, default: Date.now }
});

/**
    Create the banner
    description param is optional.
*/
bannerSchema.statics.create = (img_url, web_link, description, callback) => {
    let Banner = mongoose.model('Banner');
    new Banner({
        img_url: img_url,
        web_link: web_link,
        description: description,
        upload_time: Date.now()
    }).save((err) => {
        if(!err) {
            //  success to save
            callback({
                status: 'OK'
            });
        } else {
            //  handle error
            callback({
                status: 'error',
                message: 'db error'
            });
        }
    });
};

/**
    Delete the banner by received banner id.
*/
bannerSchema.statics.delete = (banner_id, callback) => {
    if(!banner_id) {
        callback({
            status: 'error',
            message: 'banner_id is undefined.'
        });
        return;
    }
    this.findById(banner_id).remove((err) => {
        if(!err) {
            callback({
                status: 'OK'
            });
        } else {
            //  error callback
            callback({
                status: 'error',
                message: 'db error'
            });
        }
    });
};


const searchKeywordSchema = new Schema({
    keyword: String,
    important: Number
});

const webPushSchema = new Schema({
    message: String,
    message_content: String,
    img_url: String,
    target_user_ids: [{ type: Schema.Types.ObjectId, ref: 'Member' }],
    send_time: { type: Date, default: Date.now }
});

module.exports = {
    Member: mongoose.model('Member', memberSchema),
    Post: mongoose.model('Post', postSchema),
    Message: mongoose.model('Message', messageSchema),
    Pickbook: mongoose.model('Pickbook', pickbookSchema),
    PushToken: mongoose.model('PushToken', pushTokenSchema),
    Reservation: mongoose.model('Reservation', reservationSchema),
    Request: mongoose.model('Request', requestSchema),
    Notification: mongoose.model('Notification', notificationSchema),
    ConsoleMember: mongoose.model('ConsoleMember', consoleMemberSchema),
    TattooistRequest: mongoose.model('TattooistRequest', tattooistRequestSchema),
    Banner: mongoose.model('Banner', bannerSchema),
    SearchKeyword: mongoose.model('SearchKeyword', searchKeywordSchema),
    WebPush: mongoose.model('WebPush', webPushSchema)
}
