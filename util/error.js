'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var errorSchema = new Schema({
    time: Date,
    error: String,
    message: String,
    ip: String
});

errorSchema.statics.save = (ip, error, message) => {
    let Error = mongoose.model('Error');
    new Error({
        time: Date.now(),
        error: error,
        message: message,
        ip: ip
    }).save((_err) => {
        if(!_err) {
            console.log('An error reported into Error DB.');
        }
    });
}
const Error = mongoose.model('Error', errorSchema);

module.exports.save = (ip, error, message) => {
    Error.save(ip, error, message);
}
