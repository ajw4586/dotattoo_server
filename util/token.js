var crypto = require('crypto');

/**
    Create access token to use this service.
    access token is created based on current time and user id.
    @return created access_token
*/
function createAccessToken(user_id) {
    var shasum = crypto.createHash('sha256');
    shasum.update(Date.now() + '-' + user_id);
    return shasum.digest('hex');
}

module.exports.createAccessToken = createAccessToken;
