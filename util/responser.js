module.exports.success = function(res, data) {
    if(data) {
        res.json({
            status: 'OK',
            data: data
        });
    } else {
        res.json({
            status: 'OK'
        });
    }
}
module.exports.error = function(res, message, statusCode) {
    if(statusCode) {
        res.status(statusCode).json({
            status:' error',
            message: message
        });
    } else {
        res.status(401).json({
            status: 'error',
            message: message
        });
    }
}
