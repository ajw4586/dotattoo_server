'use strict';
const fs = require('fs');

if(!process.env.S3_BUCKET || !process.env.S3_KEY || !process.env.S3_SECRET) {
    console.log('S3 Bucket is not set. Please make sure register your bucket on PATH.');
    return;
}

const AWS = require('aws-sdk');

AWS.config.region = 'ap-northeast-2';
AWS.config.update({
    accessKeyId: process.env.S3_KEY,
    secretAccessKey: process.env.S3_SECRET
});

let photoBucket = new AWS.S3();

module.exports.s3 = photoBucket;
// module.exports.uploadPhoto = function(file, callback) {
//     photoBucket.upload({
//         ACL: 'public-read',
//         Key: file.name,
//         Body: fs.createReadStream(file.path)
//     }, callback);
// }
