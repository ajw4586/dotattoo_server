'use strict';
const express = require('express');
const router = express.Router();

const Member = require('../util/db-schema').Member;
const Banner = require('../util/db-schema').Banner;
const responser = require('../util/responser');

router.get('/banners', (req, res) => {
    let access_token = req.query.access_token;

    Member.inspectAccessToken(access_token, (result) => {
        if(result.status === 'OK') {
            Banner.find().exec((err, banners) => {
                if(!err && banners) {
                    responser.success(res, banners);
                } else {
                    responser.error(res, 'Failed to get banners.', 500);
                }
            });
        } else {
            responser.error(res, 'Invalid access_token', 401);
        }
    });
});

module.exports = router;
