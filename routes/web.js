'use strict';
const USER_MANAGE = 'user_manage';
const TATTOOIST_REQUEST = 'tattooist_request';
const RESERVATION_MANAGE = 'reservation_manage';
const BANNER = 'banner';
const PUSH = 'push';
const FIND_RECOMMENDATION = 'find_recommendation';
const DECRYPT_KEY = 'dotattoo0119';

const crypto = require('crypto');
const fs = require('fs');
const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const router = express.Router();

const multerS3 = require('multer-s3');
const s3upload = require('../util/bucket');
const ConsoleMember = require('../util/db-schema').ConsoleMember;
const Member = require('../util/db-schema').Member;
const Reservation = require('../util/db-schema').Reservation;
const TattooistRequest = require('../util/db-schema').TattooistRequest;
const Banner = require('../util/db-schema').Banner;
const SearchKeyword = require('../util/db-schema').SearchKeyword;
const WebPush = require('../util/db-schema').WebPush;
const PushToken = require('../util/db-schema').PushToken;
let responser = require('../util/responser.js');


let tokenGenerator = require('../util/token').createAccessToken;

router.get('/', function(req, res) {
    let params = {
        tab: USER_MANAGE
    };
    res.render('console_login', params);
});

router.get('/console', function(req, res) {
    let tattooists = [], users = [];

    //  For tattooists detail query
    //  Find tattooist first,
    //  then add member data.
    Member.find({ type: 'tattooist' }).exec((err, members) => {
        if(!err && members) {
            members.forEach((member) => {
                tattooists.push(member)
            });
        }
        Member.find({ type: 'user' })
            .select('name social')
            .exec((err, members) => {
                if(!err && members) {
                    members.forEach((member) => {
                        return users.push(member);
                    });
                }
                let params = {
                    tab: USER_MANAGE,
                    tattooists: tattooists,
                    users: users
                };
                res.render('console', params);
            });
    });

});

/*^
 *  @api {POST} /console/login Process login for console.
 *  @apiName consoleLogin
 *  @apiGroup console
 *
 *  @apiParam (String) id id for console
 *  @apiParam (String) pw password for console
 *
 *  @apiError RedirectConsoleLogin redirect console login.
 */
router.post('/console/login', function(req, res) {
    let sha = crypto.createHash('sha256')
    let id = req.body.id;
    let pw = req.body.pw;

    //  decrypt pw field
    if(pw == undefined || id == undefined) {
        responser.error(res, 'field value is not fullfilled.', 400);
    }

    sha.update(pw);
    let hash_pw = sha.digest('hex');
    console.log(hash_pw);

    ConsoleMember.findOne({ id: id, pw: hash_pw })
        .exec((err, member) => {
            if(!err && member) {
                if(!member.signup_date) {
                    member.signup_date = Date.now();
                }
                member.access_token = tokenGenerator(hash_pw);
                member.last_login_time = Date.now();
                member.save((err) => {
                    if(!err) {
                        responser.success(res, {
                            access_token: member.access_token
                        });
                    } else {
                        responser.error(res, 'login error.', 500);
                    }
                });
            } else {
                responser.error(res, 'login failed.', 401);
            }
        });

});

router.get('/tattooist/request', function(req, res) {
    let param = {
        tab: TATTOOIST_REQUEST
    };
    TattooistRequest.find()
        .populate('request_user_id')
        .lean()
        .exec((err, requests) => {
        if(!err && requests) {
            param.data = requests;
            res.render('tattooist_request', param);
        } else {
            responser.error(res, 'Failed to find any tattooist request.', 404);
            param.data = []
            res.render('tattooist_request', param);
        }
    });
});

router.get('/reservation', function(req, res) {
    let results = [];
    Reservation.find()
        .populate('request')
        .lean()
        .exec((err, reservations) => {
            if(!err && reservations) {
                let populationOption = [{
                    path: 'request.tattooist_id',
                    select: 'name'
                },{
                    path: 'request.user_id',
                    select: 'name'
                }];
                Member.populate(reservations, populationOption, (err, reservations) => {
                    if(!err && reservations) {
                        results = reservations;
                        console.log(results);
                        let param = {
                            tab: RESERVATION_MANAGE,
                            reservation: results
                        };
                        res.render('reservation', param);
                    }
                });
            } else {
                let param = {
                    tab: RESERVATION_MANAGE
                };
                res.render('reservation', param);
            }
        });
});

let bannerUpload = multer({
    storage: multerS3({
        s3: s3upload.s3,
        bucket: 'dotattoo',
        acl: 'public-read',
        metadata: (req, file, cb) => {
            cb(null, { description: req.body.description });
        },
        key: (req, file, cb) => {
            let name = 'images/banner/' + 'banner.' + Date.now().toString();
            switch (file.fieldname) {
                case 'banner_img':
                    req.body.banner_img_key = name;
                    break;
                case 'detail_img':
                    req.body.detail_img_key = name;
                    break;
            }
            cb(null, name);
        },
        contentType: multerS3.AUTO_CONTENT_TYPE
    })
});

router.route('/console/banner')
    .get((req, res) => {
        let param = {
            tab: BANNER
        };
        Banner.find()
            .lean()
            .exec((err, banners) => {
                if(!err && banners) {
                    param.data = banners;
                } else {
                    param.data = [];
                }
                res.render('banner', param);
            });
    })
    /**
        @api {POST} /console/banner add banner
        @apiName add banner API
        @apiGroup console

        @apiParam {String} access_token console user's access_token
        @apiParam {String} banner_img banner image url
        @apiParam {String} detail_img banner detail image
        @apiParam {String} description banner description

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /console/banner
    */
    .post(bannerUpload.fields([{ name: 'banner_img' }, { name: 'detail_img' }]), (req, res) => {
        let access_token = req.body.access_token;
        let description = req.body.description;
        let banner_img_path = 'https://s3.ap-northeast-2.amazonaws.com/dotattoo/' + req.body.banner_img_key;
        let detail_img_path = 'https://s3.ap-northeast-2.amazonaws.com/dotattoo/' + req.body.detail_img_key;

        ConsoleMember.inspectConsoleAccessToken(access_token, (result) => {
            if(result.status === 'OK') {
                new Banner({
                    img_url: banner_img_path,
                    web_link: detail_img_path,
                    description: description,
                    upload_time: Date.now()
                }).save((err) => {
                    if(!err) {
                        //  success to save
                        responser.success(res);
                    } else {
                        //  handle error
                        responser.error(res, 'internal db error.', 500);
                    }
                });
            } else {
                //  error response
                responser.error(res, 'Invalid access_token.', 401);
            }
        });

    })
    /**
        @api {DELETE} /console/banner delete banner
        @apiName delete banner API
        @apiGroup console

        @apiParam {String} access_token console user's access_token
        @apiParam {String} banner_id what you want to delete.

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /console/banner
    */
    .delete((req, res) => {
        const access_token = req.body.access_token;
        const banner_id = req.body.banner_id;

        ConsoleMember.inspectConsoleAccessToken(access_token, (result) => {
            if(result.status === 'OK') {
                Banner.findById(new mongoose.Types.ObjectId(banner_id)).remove((err, banner) => {
                    if(!err && banner) {
                        responser.success(res);
                    } else {
                        //  not found
                        responser.error(res, 'Unable to find by given banner_id.', 404);
                    }
                });
            } else {
                //  error response
                responser.error(res, 'Invalid access_token', 401);
            }
        });
    });

router.get('/console/push', (req, res) => {
    let param = {
        tab: PUSH
    };

    WebPush.find().exec((err, pushes) => {
        if(!err && pushes) {
            param.data = pushes;
        } else {
            //  error response
            param.data = [];
        }
        PushToken.find()
            .populate('user_id')
            .exec((err, tokens) => {
                if(!err && tokens) {
                    param.token = tokens;
                } else {
                    param.token = [];
                }
                res.render('push', param);
            });
    });
});

/**
    @api {POST} /console/tattooist/state_change change tattooist state
    @apiName changeTattooistState
    @apiGroup tattooist

    @apiParam {String} access_token web console user's access_token
    @apiParam {String="contact","consult"} type change state type
    @apiParam {String} target_user_id user_id that you want to change.

    @apiSuccess {String} status

    @apiError {String} status
    @apiError {String} message

    @apiSampleRequest /console/tattooist/state_change
*/
router.post('/console/tattooist/state_change', (req, res) => {
    const access_token = req.body.access_token;
    const type = req.body.type;
    const target_user_id = req.body.target_user_id;

    if(!type === 'contact' || !type === 'consult') {
        responser.error(res, 'Invalid type parameter.', 400);
        return;
    }

    ConsoleMember.inspectConsoleAccessToken(access_token, (result) => {
        if(result.status == 'OK') {
            Member.findUserById(new mongoose.Types.ObjectId(target_user_id), (result) => {
                if(result.status == 'OK') {
                    switch (type) {
                        case 'contact':
                            result.user.tattooist.status.show_contact = !result.user.tattooist.status.show_contact;
                            break;
                        case 'consult':
                            result.user.tattooist.status.show_consult = !result.user.tattooist.status.show_consult;
                            break;
                    }
                    result.user.save((err) => {
                        if(!err) {
                            let cur_state;
                            switch (type) {
                                case 'contact':
                                    cur_state = result.user.tattooist.status.show_contact;
                                    break;
                                case 'consult':
                                cur_state = result.user.tattooist.status.show_consult;
                                    break;
                            }

                            //  success to save
                            responser.success(res, {
                                state: cur_state
                            });
                        } else {
                            //  handle error
                            responser.error(res, 'Cannot change tattooist state.', 500);
                        }
                    });
                } else {
                    // error response
                    responser.error(res, 'Cannot find any user.', 404);
                }
            })
        } else {
            responser.error(res, 'Invalid access_token', 401);
        }
    });
});

router.route('/find_recommendation')
    .get((req, res) => {
        let param = {
            tab: FIND_RECOMMENDATION
        };
        res.render('find_recommendation', param);
    });

module.exports = router;
