    "use strict";
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multiparty = require('multiparty');
const multer = require('multer');
const multerS3 = require('multer-s3');
const s3ipload = require('../util/bucket');

const Member = require('../util/db-schema').Member;
const Message = require('../util/db-schema').Message;
const PushToken = require('../util/db-schema').PushToken;

const responser = require('../util/responser');
let parseDate = require('../util/utils').parseDate;

//  get message list from all user
//
//  Query structure
// First: Query messages that match 'from' field user_id
//  Second: Then, Query again, but for 'to' field user_id and except first query's results.
//  Finally: populate user_id and response
router.get('/message/list', (req, res) => {
    let access_token = req.query.access_token;

    Member.inspectAccessToken(access_token, (result) => {
        if(result.status == 'OK') {
            //  Query messages
            Message.aggregate([{
                $match: {
                    from: result.user_id
                }},
                { $group: {
                    _id: {
                        from: '$from',
                        to: '$to',
                    },
                    time: {
                        $last: '$time'
                    },
                    type: {
                        $last: '$type'
                    },
                    message: {
                        $last: '$message'
                    }
                }},
                { $project: {
                    _id: 0,
                    from: '$_id.from',
                    to: '$_id.to',
                    type: '$type',
                    time:'$time',
                    message: '$message'
                }}]).exec((err, fromMessages) => {
                    if(err) {
                        console.error(err);
                        responser.error(res, 'internal error.', 500);
                    }
                    let except = [];
                    // fromMessages.forEach((message) => {
                    //     return except.push({ from: { $ne: message.to }, to: result.user_id });
                    // });
                    fromMessages.forEach((message) => {
                        return except.push(message.to);
                    });
                    Message.aggregate([{
                        $match: {
                            from: { $nin: except },
                            to: result.user_id
                        }
                    },
                    { $group: {
                        _id: {
                            from: '$from',
                            to: '$to',
                        },
                        time: {
                            $last: '$time'
                        },
                        type: {
                            $last: '$type'
                        },
                        message: {
                            $last: '$message'
                        }
                    }},
                    { $project: {
                        _id: 0,
                        from: '$_id.from',
                        to: '$_id.to',
                        type: '$type',
                        time:'$time',
                        message: '$message'
                    }
                }]).exec((err, toMessages) => {
                    if(err) {
                        console.error(err);
                        responser.error(res, 'internal error.', 500);
                    }
                    //  merge two array
                    toMessages.forEach((toResult) => {
                        fromMessages.push(toResult);
                    });
                    //  sort by sent time
                    for(let idx = fromMessages.length; idx > 1; idx--) {
                        console.log('index: ' + idx);
                        // if is not, the index is last and sort is done
                        for(let innerIdx = 0; innerIdx < idx; innerIdx++) {
                            if(fromMessages.length-2 >= innerIdx) {
                                let targetTime = new Date(fromMessages[innerIdx].time);
                                let compareTime = new Date(fromMessages[innerIdx+1].time);
                                if(targetTime.getTime() < compareTime.getTime()) {
                                    let tempMessage = fromMessages[innerIdx+1];
                                    fromMessages[innerIdx+1] = fromMessages[innerIdx];
                                    fromMessages[innerIdx] = tempMessage;
                                }
                            }
                        }
                    }
                    //  populate value
                    const userPopulationOptions = [
                        { path: 'from', select: 'name profile_img', model: 'Member' },
                        { path: 'to', select: 'name profile_img', model: 'Member' }
                    ];
                    Member.populate(fromMessages, userPopulationOptions, (err, finalMessages) => {
                        if(!err && finalMessages) {
                            responser.success(res, fromMessages);
                        } else {
                            responser.error(res, 'Error while populating messages user information.', 500);
                        }
                    });
                });
            });
        } else {
            //  Error response
            responser.error(res, 'Failed to inspect access token.', 401);
        }
    });
});

router.route('/message/:type')
    //  send message to specified user_id.
    .post((req, res) => {
        let message_type = req.params.type;

        //  start handle by message_type.
        //
        //  according to the message type,
        //  get default params for each way.
        if(message_type == 'text') {
            receiveBody();
        } else if(message_type == 'photo') {
            receivePart();
        } else if(message_tpye == 'request') {
            receiveRequest();
        } else if(message_type == 'app_link') {
            receiveAppLink();
        } else {
            responser.error(res, 'invalid message type.');
        }

        //  handle photo message
        function receivePart() {
            let form = new multiparty.Form({
                uploadDir: './images/messages'
            });

            //  params
            let access_token;
            let user_id;
            let image_url;

            form.on('file', function(name, file) {
                if(name == 'message') {
                    //  substring file name into file path
                    image_url = 'http://52.79.95.175/messages/' + file.path.substring(16, file.path.length);
                    console.log('file name: ' + file.path.substring(16, file.path.length));
                }
            });
            form.on('field', function(name, value) {
                switch(name) {
                    case 'access_token':
                        access_token = value;
                        break;
                    case 'target_user_id':
                        user_id = value;
                        break;
                }
            });
            //  call Message Schema's API
            //  after collect all information
            form.on('close', function() {
                //  First, token inspection
                //  and validate target user.
                //  and then, call Message Schema API
                Member.inspectAccessToken(access_token, (result) => {
                    if(result.status == 'OK') {
                        let my_user_id = result.user_id;
                        Member.findUserById(user_id, (result) => {
                            if(result.status == 'OK') {
                                //  perform saving
                                Message.photoMessage(my_user_id, new mongoose.Types.ObjectId(user_id), image_url, (result) => {
                                    if(result.status == 'OK') {
                                        responser.success(res, result.time);
                                    } else {
                                        //  Error response
                                        responser.error(res, 'Unable to send message.', 500);
                                    }
                                });
                            } else {
                                //  Error response
                                responser.error(res, 'Unable to find target user.', 404);
                            }
                        })
                    } else {
                        //  Error response
                        responser.error(res, 'Failed to inspect access token.', 403);
                    }
                })
            });

            //  get form data
            form.parse(req);
        }
        //  handle text message
        function receiveBody() {
            let access_token = req.body.access_token;
            let user_id;
            let target_user_id = req.body.target_user_id;
            let message = req.body.message;

            //  Need token inspection and target user's id.
            Member.inspectAccessToken(access_token, (result) => {
                if(result.status == 'OK') {
                    user_id = result.user_id;
                    //  check if target user is exist.
                    Member.findUserById(new mongoose.Types.ObjectId(user_id), (result) => {
                        if(result.status == 'OK') {
                            //  perform save message
                            Message.textMessage(user_id, new mongoose.Types.ObjectId(target_user_id), message, (result) => {
                                if(result.status == 'OK') {
                                    //   response
                                    responser.success(res, result.time);
                                } else {
                                    //  Error response
                                    responser.error(res, 'db save error.', 500);
                                }
                            });
                        } else {
                            //  Error response
                            responser.error(res, 'Unable to find message target user.', 404);
                        }
                    });
                } else {
                    //  Error response
                    responser.error(res, 'access_token is invalid.');
                }
            });
        }
        function receiveAppLink() {
            let access_token = req.body.access_token;
            let user_id;
            let target_user_id = req.body.target_user_id;
            let link = req.body.link;

            //  Need token inspection and target user's id.
            Member.inspectAccessToken(access_token, (result) => {
                if(result.status == 'OK') {
                    user_id = result.user_id;
                    //  check if target user is exist.
                    Member.findUserById(new mongoose.Types.ObjectId(user_id), (result) => {
                        if(result.status == 'OK') {
                            //  perform save message
                            Message.textMessage(user_id, new mongoose.Types.ObjectId(target_user_id), message, (result) => {
                                if(result.status == 'OK') {
                                    //   response
                                    responser.success(res, result.time);
                                } else {
                                    //  Error response
                                    responser.error(res, 'db save error.', 500);
                                }
                            });
                        } else {
                            //  Error response
                            responser.error(res, 'Unable to find message target user.', 404);
                        }
                    });
                } else {
                    //  Error response
                    responser.error(res, 'access_token is invalid.');
                }
            });
        }
        function receiveRequest() {
            let access_token = req.body.access_token;
            let user_id;
            let target_user_id = req.body.target_user_id;
            let request_id = req.body.request_id;

            Member.inspectAccessToken(access_token, function(result) {
                if(result.status == 'OK') {
                    user_id = result.user_id;
                    Message.requestMessage(user_id, new mongoose.Types.ObjectId(receive_user_id),
                                    new mongoose.Types.ObjectId(request_id),
                                    function(result) {
                                        if(result.status == 'OK') {
                                            responser.success(res);
                                        } else {
                                            responser.error(res, result.message);
                                        }
                                    });
                } else {
                    responser.error(res, 'Failed to inspect access_token');
                }
            });
        }
    });

    //  get messages from user_id.
    router.get('/message/:user_id', (req, res) => {
        let access_token = req.query.access_token;
        let user_id;
        let target_user_id = req.params.user_id;

        //  token inspection and validate user_id.
        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                //  set user_id
                user_id = result.user_id;
                //  query target user's received/sent messages.
                Member.findUserById(new mongoose.Types.ObjectId(target_user_id), (result) => {
                    if(result.status == 'OK') {
                        //  Query messages between user and target user.
                        Message.find().or([{
                                from: new mongoose.Types.ObjectId(target_user_id),
                                to: user_id
                            }, {
                                from: user_id,
                                to: new mongoose.Types.ObjectId(target_user_id)
                        }]).exec((err, messages) => {
                            if(!err && messages) {
                                //  populate if there is any app_link
                                messages.forEach((message) => {
                                    // if()
                                });
                                //   response
                                responser.success(res, messages);
                            } else {
                                //  Error response
                                responser.error(res, 'Unable to find message.', 404);
                            }
                        });
                    } else {
                        //  Error response
                        responser.error(res, 'target user is not valid.', 404);
                    }
                });
            } else {
                //  Error response
                responser.error(res, 'access token is invalid.');
            }
        });
    });

module.exports = router;
