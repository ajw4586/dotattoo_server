"use strict";
let express = require('express');
let router = express.Router();
let Post = require('../util/db-schema').Post;
let Member = require('../util/db-schema').Member;

/**
    Search API

    @api {GET} /search Post & User Search API
    @apiGroup search

    @apiParam {String="post", "user"} search_type
    @apiParam {String} key

    @apiSampleRequest /search

    @param search_type: specify search type for returns.
    @param key: what you want to search
*/
router.get('/search', function(req, res) {
    var key = req.query.key;
    var search_type = req.query.search_type;

    if(search_type == 'post') {
        searchPost(key, req, res);
    } else if(search_type == 'user') {
        searchUser(key, req, res);
    } else {
        error(res, 'bad request.', 400);
    }

    function searchPost(key, req, res) {
        //  search post by keyword
        Post.find({ $or: [{ name: new RegExp(key, 'i')},
            { title: new RegExp(key, 'i')},
            { comment: new RegExp(key, 'i')},
            { part: new RegExp(key, 'i')}
        ]})
        //  sort by recent post
        .sort(
            { 'info.post_time': -1, 'info.popularity': -1 }
        )
        .populate('info.user_id', 'name profile_img')
        .exec(function(err, posts) {
            if(!err && posts) {
                success(res, posts);
            } else {
                //  no data
                error(res, 'No post data.', 404);
            }
        });
    }
    function searchUser(key, req, res) {
        //  search user by keyword
        Member.find({ name: new RegExp(key, 'i') })
        .exec(function(err, users) {
            if(!err && users) {
                success(res, users);
            } else {
                error(res, 'No user found.', 404);
            }
        });
    }

    function success(res, result) {
        res.json({
            status: 'OK',
            data: result
        });
    }
    function error(res, message, statusCode) {
        if(statusCode) {
            res.status(statusCode).json({
                status: 'error',
                message: message
            });
        } else {
            res.status(400).json({
                status: 'error',
                message: message
            });
        }
    }
});

module.exports = router;
