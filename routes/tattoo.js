'use strict';
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multiparty = require('multiparty');

const Member = require('../util/db-schema').Member;
const Message = require('../util/db-schema').Message;
const Request = require('../util/db-schema').Request;
const Reservation = require('../util/db-schema').Reservation;
const ConsoleMember = require('../util/db-schema').ConsoleMember;
const TattooistRequest = require('../util/db-schema').TattooistRequest;
const responser = require('../util/responser');

let parseDate = require('../util/utils').parseDate;


router.route('/tattoo/tattooist/register')
    /**
        @api {POST} /tattoo/tattooist/register turn user to tattooist.
        @apiName registerTattooist
        @apiGroup tattoo

        @apiParam {String} access_token
        @apiParam {String} target_user_id
        @apiParam {String} real_name
        @apiParam {String} place
        @apiParam {String} address
        @apiParam {String} email
        @apiParam {String} phone

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /tattoo/tattooist/register
    */
    .post((req, res) => {
        let access_token = req.body.access_token;
        let request_id = req.body.request_id;
        let request_user_id = req.body.request_user_id;
        let real_name = req.body.real_name;
        let place = req.body.place;
        let address = req.body.address;
        let email = req.body.email;
        let phone = req.body.phone;
        let introduction = req.body.introduction;
        let bank_name = req.body.bank_name;
        let bank_account = req.body.bank_account;
        let bank_owner = req.body.bank_owner;

        ConsoleMember.inspectConsoleAccessToken(access_token, (result) => {
            if(result.status == 'OK') {
                Member.findOne({ _id: new mongoose.Types.ObjectId(request_user_id), type: 'user' })
                    .exec((err, member) => {
                        if(!err && member) {
                            member.type = 'tattooist';
                            member.tattooist = {
                                name: real_name,
                                place: place,
                                address: address,
                                introduction: introduction,
                                bank_account: {
                                    name: bank_name,
                                    account: bank_account,
                                    owner: bank_owner
                                },
                                status: {
                                    show_contact: true,
                                    show_consult: true
                                }
                            };
                            member.email = email;
                            member.phone = phone;
                            member.save((err) => {
                                if(!err) {
                                    //  success to save
                                    //  make request status to done.
                                    TattooistRequest.done(new mongoose.Types.ObjectId(request_id));
                                    responser.success(res);
                                } else {
                                    //  handle error
                                    responser.error(res,
                                        'Failed to save tattooist data while turning user to tattooist.', 500);
                                }
                            });
                        } else {
                            responser.error(res,
                                'Failed to find user to turn to tattooist.', 404);
                        }
                    });
            } else {
                responser.error(res, 'Invalid access_token', 401);
            }
        });
    });

router.route('/tattoo/tattooist/request')
    /**
        @api /tattoo/tattooist/request request to turn user to tattooist.
        @apiName tattooist request api
        @apiGroup tattoo
        @apiDescription request to turn user to a tattooist account

        @apiParam {String} access_token
        @apiParam {String} real_name
        @apiParam {String} place
        @apiParam {String} address
        @apiParam {String} email
        @apiParam {String} phone
        @apiParam {String} introduction

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /tattoo/tattooist/request
    */
    .post((req, res) => {
        let access_token = req.body.access_token;
        let real_name = req.body.real_name;
        let place = req.body.place;
        let address = req.body.address;
        let email = req.body.email;
        let phone = req.body.phone;
        let introduction = req.body.introduction;
        let bank_account = req.body.bank_account;
        let bank_name = req.body.bank_name;
        let bank_owner = req.body.bank_owner;

        Member.inspectAccessToken(access_token, (result) => {
            if(result.status == 'OK') {
                let request = new TattooistRequest({
                    request_user_id: result.user_id,
                    real_name: real_name,
                    place: place,
                    address: address,
                    email: email,
                    phone: phone,
                    introduction: introduction,
                    requested_time: Date.now(),
                    bank: {
                        account: bank_account,
                        name: bank_name,
                        owner: bank_owner
                    }
                });
                request.save((err) => {
                    if(!err) {
                        //  success to save
                        responser.success(res);
                    } else {
                        //  handle error
                        responser.error(res, 'Failed to save tattooist request.', 500);
                    }
                });
            } else {
                responser.error(res, 'Invald access_token.', 401);
            }
        });
    });


//  delete tattooist register request
router.post('/tattoo/tattooist/request/delete', (req, res) => {
    const access_token = req.body.access_token;
    const request_id = req.body.request_id;

    ConsoleMember.inspectConsoleAccessToken(access_token, (result) => {
        if(result.status == 'OK') {
            TattooistRequest.find({ _id: new mongoose.Types.ObjectId(request_id) })
                .remove().exec((err, removed) => {
                    if(!err, removed) {
                        //  success
                        responser.success(res)
                    } else {
                        //  failed to remove
                        responser.error(res, 'Failed to remove tattooist request.', 500)
                    }
                });
        } else {
            //  error response
            responser.error(res, 'access_token is invalid.', 401)
        }
    });
});

router.route('/tattoo/reservation')
    /**
        @api {GET} /tattoo/reservation get tattoo reservation
        @apiName getReservation
        @apiDescription get one tattoo reservation by given reservation id.
        @apiGroup tattoo

        @apiParam {String} reservation_id

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /tattoo/reservation
    */
    .get((req, res) => {
        let reservation_id = req.query.reservation_id;

        Reservation.findOne({ _id: new mongoose.Types.ObjectId(reservation_id) })
            .exec((err, reservation) => {
                if(!err && reservation) {
                    responser.success(res, reservation);
                } else {
                    responser.error(res, 'Unable to find reservation.', 404);
                }
            });
    })
    /**
        @api {POST} /tattoo/reservation upload tattoo reservation
        @apiName reservateTattoo
        @apiDescription upload tattoo reservation using received request
        from user.
        @apiGroup tattoo

        @apiParam {String} access_token
        @apiParam {String} request_id
        @apiParam {String} birthday
        @apiParam {String} phone_num
        @apiParam {String} user_name

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /tattoo/reservation
    */
    .post(function(req, res) {
        let access_token = req.body.access_token;
        let request_id = req.body.request_id;
        let birthday = req.body.birthday;
        let phone_num = req.body.phone_num;
        let user_name = req.body.user_name;

        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                //  process save reservation
                let reservation = new Reservation({
                    request: new mongoose.Types.ObjectId(request_id),
                    purchase_user_info: {
                        birthday: parseDate(birthday),
                        phone_num: phone_num,
                        user_name: user_name
                    }
                });
                reservation.save(function(err) {
                    if(!err) {
                        responser.success(res);
                    } else {
                        responser.error(res, 'An error occured while saving reservation into db.', 500);
                    }
                });
            } else {
                responser.error(res, 'Failed to inspect access_token.');
            }
        })
    });

/**
    @api {GET} /tattoo/reservation/list show reservation list
    @apiName getReservationList
    @apiDescription get tattoo reservation list related by given access_token
    @apiGroup tattoo

    @apiParam {String} access_token
    @apiSuccess {String} status
    @apiSuccess {String} data

    @apiError {String} status
    @apiError {String} message

    @apiSampleRequest /tattoo/reservation/list
*/
router.get('/tattoo/reservation/list', function(req, res) {
    let access_token = req.query.access_token;

    Member.inspectAccessToken(access_token, function(result) {
        if(result.status == 'OK') {
            //  show all reservations.
            Reservation.find()
            .populate('request')
            .populate({
                path: 'Request',
                match: { tattooist_id: result.user_id }
            })
            .populate({
                path: 'Request',
                match: { user_id: result.user_id }
            })
            .sort({ reservation_time: -1 })
            .exec(function(err, reservations) {
                if(!err && reservations) {
                    reservations = reservations.filter(function(reservations) {
                        return
                    });
                    responser.success(res, reservations);
                } else {
                    responser.error(res, 'No reservations are found.', 404);
                }
            });
        } else {
            responser.error(res, 'Failed to inspect access_token.');
        }
    });
});

router.route('/tattoo/request')
    .get(function(req, res) {
        let request_id = req.query.request_id;

        Request.findOne({ _id: new mongoose.Types.ObjectId(request_id) })
        .populate('tattooist_id')
        .populate('user_id')
        .exec(function(err, request) {
            if(!err && request) {
                responser.success(res, request);
            } else {
                responser.error(res, 'No requests are found.', 404);
            }
        });
    })
    .post(function(req, res) {

        let access_token = req.body.access_token;
        let tattooist_id = req.body.tattooist_id;
        let user_id = req.body.user_id;
        let tattoo_name = req.body.tattoo_name;
        let genre = req.body.genre;
        let part = req.body.part;
        let place = req.body.place;
        let date = req.body.date;
        let term = req.body.term;
        let price = req.body.price;
        let photo;


        let form = new multiparty.Form({
            uploadDir: './images',
        });
        form.on('file', function(name, file) {
            if(name == 'photo') {
                photo = 'http://52.79.95.175/'
                + file.path.substring(7, file.path.length);
            }
        });
        form.on('field', function(name, value) {
            switch (name) {
                case 'access_token':
                    access_token = value;
                    break;
                case 'tattooist_id':
                tattooist_id = value;
                    break;
                case 'user_id':
                    user_id = value;
                    break;
                case 'tattoo_name':
                    tattoo_name = value;
                    break;
                case 'genre':
                    genre = value;
                    break;
                case 'part':
                    part = value;
                    break;
                case 'place':
                    place = value;
                    break;
                case 'date':
                    date = value;
                    break;
                case 'term':
                    term = value;
                    break;
                case 'price':
                    price = value;
                    break;
            }
        });
        form.on('close', function() {
            Member.inspectAccessToken(access_token, function(result) {
                if(result.status == 'OK') {
                    let request = new Request({
                        tattooist_id: new mongoose.Types.ObjectId(tattooist_id),
                        user_id: new mongoose.Types.ObjectId(user_id),
                        info: {
                            photo: photo,
                            tattoo_name: tattoo_name,
                            genre: genre,
                            place: place,
                            part: part,
                            date: date,
                            price: price,
                            reservation_price: price * 0.1
                        }
                    });
                    request.save(function(err) {
                        if(!err) {
                            Message.requestMessage(request.user_id, request.tattooist_id, request, function(result) {
                                if(result.status == 'OK') {
                                    responser.success(res);
                                } else {
                                    responser.error(res, 'Cannot send message but request is sent.', 500);
                                }
                            });
                        } else {
                            responser.error(res, 'An error occured while saving request into db.', 500);
                        }
                    });
                } else {
                    responser.error(res, 'Failed to inspect access_token.');
                }
            });
        });
        form.parse(req);

    });
router.get('/tattoo/request/list', function(req, res) {
    let access_token = req.query.access_token;

    Member.inspectAccessToken(access_token, function(result) {
        if(result.status == 'OK') {
            //  show all requests.
            Request.find({ $or: [{ tattooist_id: result.user_id }, { user_id: result.user_id }] })
            .populate('tattooist_id', 'name profile_img')
            .populate('user_id', 'name profile_img')
            .sort({ requested_time: -1 })
            .exec(function(err, requests) {
                if(!err && requests) {
                    responser.success(res, requests);
                } else {
                    responser.error(res, 'No requests are found.', 404);
                }
            });
        } else {
            responser.error(res, 'Failed to inspect access_token.');
        }
    });
});

module.exports = router;
