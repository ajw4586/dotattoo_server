'use strict';
const APP_ACCESS_TOKEN = '208716156141616|S-b4oThWMZAg7pwtu2q2Lu2GOgI';
const KAKAO_GET_USER_TOKEN = 'kauth.kakao.com/oauth/token';
const KAKAO_REST_API_KEY = '159dc87a911c2a93bb81a783fb1d3790';
const KAKAO_REDIRECT_URL = 'http://52.79.95.175/login';
const KAKAO_REQUEST_USER_INFO = 'https://kapi.kakao.com/v1/user/me';
const KAKAO_API_ADMIN_KEY = '8d2b8f24c0e10bb4b11f983a2c6f6e4b';
const INSPECT_KAKAO_ACCESS_TOKEN_URL = 'https://kapi.kakao.com/v1/user/access_token_info';

const express = require('express');
const router = express.Router();
const request = require('request');
const mongoose = require('mongoose');
const multer = require('multer');
const multerS3 = require('multer-s3');

const s3upload = require('../util/bucket');
const responser = require('../util/responser');
const tokenGenerator = require('../util/token').createAccessToken;
const errorReporter = require('../util/error');

const Member = require('../util/db-schema').Member;
const Post = require('../util/db-schema').Post;
const Error = require('../util/db-schema').Error;

router.route('/login')
    /**
        @api {POST} /login login API for facebook and kakao. If user is not exist, then create new user and login.
        @apiGroup login

        @apiParam {String="facebook", "kakao"} type login type for users
        @apiParam {String} access_token facebook/kakao access_token to get basic user information

        @apiSuccess {String} status It will be 'OK'
        @apiSuccess {String} message
        @apiSuccess {String} user_id our service's user_id
        @apiSuccess {String} access_token our service's access_token

    */
    .post((req, res) => {
        //  handle login by type params
        var type = req.body.type;
        console.log('login request type: ' + type);
        if(type == 'facebook') {
            //  Facebook login
            oauthFB(req, res);
        } else if(type == 'kakao') {
            //  kakao login
            loginKakao(req, res);
        } else if(type == 'phone') {
            //  phone number login
        } else {
            loginError(req, res);
        }
        function loginKakao(req, res) {
            let access_token = req.body.access_token;
            inspectKakaoAccessToken(access_token, function(data) {
                if(!data) {
                    return;
                } else {
                    //  Query user already exists.
                    Member.findOne({ 'social.kakao.id': data.id })
                    .exec((err, user) => {
                        if(err) {
                            errorKakaoLogin(res);
                        } else if(user) {
                            performLoginKakao(user);
                        } else {
                            //  create user_data
                            requestKakaoUserInfo(access_token, data.id, function(user_data) {
                                console.log(user_data);
                                createKakaoAccount(user_data, access_token);
                            });
                        }
                    });
                }
            });

        }
        function performLoginKakao(user) {
            user.access_token = tokenGenerator(user._id);
            user.save((err) => {
                if(!err) {
                    successKakaoLogin(res, user)
                } else {
                    errorKakaoLogin(res);
                }
            });
        }
        function successKakaoLogin(res, data) {
            res.json({
                status: 'OK',
                message: 'Succeeded to login by kakao.',
                user_id: data._id,
                access_token: data.access_token
            });
        }
        function errorKakaoLogin(res) {
            res.status(401).json({
                status: 'error',
                message: 'Failed to login kakao.'
            });
        }

        //  Inspect kakao Access token.
        //  if it is valid, perform login process.
        function inspectKakaoAccessToken(access_token, callback) {
            request({
                method: 'GET',
                url: INSPECT_KAKAO_ACCESS_TOKEN_URL,
                headers: {
                    'Authorization': 'Bearer ' + access_token
                }
            })
            .on('error', (err) => {
                errorKakaoLogin(res);
                callback();
            })
            .on('response', (response) => {
                if(response.statusCode != 200) {
                    errorKakaoLogin(res);
                    callback();
                }
            })
            .on('data', (data) => {
                callback(JSON.parse(data));
            });
        }

        function createKakaoAccount(user_data, access_token) {
            let id = user_data.id;
            let name = user_data.properties.nickname;
            let profile_img = user_data.properties.profile_image;

            let kakao_user = new Member({
                name: name,
                profile_img: profile_img,
                social: {
                    kakao: {
                        id: id,
                        token: access_token
                    }
                }
            });
            kakao_user.save((err, user) => {
                if(!err && user) {
                    performLoginKakao(user);
                } else {
                    errorKakaoLogin(res);
                }
            });
        }
        function requestKakaoUserInfo(access_token, user_id, callback) {
            let user_info_request = {
                method: 'POST',
                url: KAKAO_REQUEST_USER_INFO,
                headers: {
                    'Authorization': 'Bearer ' + access_token
                },
                form: {
                    target_id_type: 'user_id',
                    target_id: user_id
                }
            }
            request(user_info_request)
            .on('error', (err) => {
                errorKakaoLogin(res);
                callback();
            })
            .on('data', (data) => {
                callback(JSON.parse(data));
            })
            .on('response', (response) => {
                if(response.statusCode != 200) {
                    errorKakaoLogin(res);
                    callback();
                }
            });
        }

    });

/**
    Get user profile information.

    @api {GET} /user/:user_id get user profile information
    @apiName getUserProfile
    @apiGroup user

    @apiParam {String} user_oid want to get user's id

    @apiSuccess {String} status It will be 'OK'

*/
router.get('/user/:user_id', (req, res) => {
    let user_oid = new mongoose.Types.ObjectId(req.params.user_id);
    Member.findOne({ '_id': user_oid })
            .select('name profile_img phone type tattooist')
            .lean()
            .exec(function(err, user) {
        //  Send error response
        if(err) {
            getUserError(res, 'An database exception occured while querying user information.', 500);
            reportError(req, err, 'An database exception occured while querying user information.');
        } else if(!user) {
            getUserError(res, 'No user found that matches given id.', 404);
            reportError(req, 'Failed to find user.', 'No user found that matches given id.');
        } else {
            Post.find({ 'info.user_id': user_oid })
                    .select('photo_url')
                    .lean()
                    .exec(function(err, posts) {
                        user['status'] = 'OK';
                        if(!err) {
                            //  add post data.
                            user['posts'] = posts;
                        }
                        //  response data
                        getUserSuccess(res, user);
                    });
        }
    });

    function getUserSuccess(res, user) {
        res.json(user);
    }
    function getUserError(res, reason, statusCode) {
        if(statusCode) {
            res.status(401).json({
                status: 'error',
                message: reason
            });
        } else {
            res.status(statusCode).json({
                status: 'error',
                message: reason
            });
        }
    }

});


function loginSuccess(res, user_id, access_token) {
    res.json({
        status: 'OK',
        message: 'Succeeded to login.',
        user_id: user_id,
        access_token: access_token
    });
}

function loginError(request, response, message) {
    errorReporter.save(request.userIp, 'An exception occured when login API requested.', message);

    if(message == undefined) {
        response.status(400).send({
            'status': 'error',
            'message': 'an error occured while login process.'
        });
    } else {
        response.status(400).send({
            'status': 'error',
            'message': message
        });
    }
}

function reportError(req, err, message, res) {
    var error = new Error({
        time: Date.now(),
        error: err,
        ip: req.userIp
    });
    error.save((_err) => {
        if(_err) {
            console.error.bind(console, 'An error occured while saving error report.\n' + err);
            return;
        }
    });
    if(res) {
        res.status(400).json({
            status: 'error',
            message: 'An error occured. Please check your request.'
        });
    }
}

/**
    process OAuth authentication to facebook server.
*/
function oauthFB(req, res) {
    var access_token = req.body.access_token;
    var DEBUG_TOKEN_API = 'https://graph.facebook.com/debug_token?'
    + 'input_token=' + access_token
    + '&access_token=' + APP_ACCESS_TOKEN;
    request.get(DEBUG_TOKEN_API)
        //  Report OAuth auth error
        .on('error', (err) => {
            reportError(req, err, 'OAuth authentication failure will report to error db.', res);
        })
        .on('response', (response) => {
            //  OAuth Failure
            //  send error response
            if(response.statusCode != 200) {
                loginError(req, res, 'OAuth authentication failed. Check access token');
            }
        })
        //
        .on('data', (data) => {
            //  compare requested user_id to API resolved user_id.
            //  Then perform login.
            var resolved = JSON.parse(data);
            if(resolved.data.is_valid == true
                && resolved.data.user_id == req.body.user_id) {
                //  Perform login
                loginFB(req, res);
            } else {
                loginError(req, res, 'User id doesn\'t match. Use valid parameter.');
            }
        });
}

/**
    Perform login facebook account.
    if user is not exist, then create user and re-call this login process.
    @return login result
*/
function loginFB(req, res) {
    Member.findOne({ 'social.facebook.id': req.body.user_id })
        .exec(function(err, member) {
            if(err) {
                reportError(req, err, 'A database query user exception will report to error db.', res);
            }
            //  if user is not exist
            //  then create an user account
            //  from given user information
            if(!member) {
                createFBAccount(req, res);
            } else {
                member.access_token = tokenGenerator(member.social.facebook.id);
                member.save((err) => {
                    if(err)
                        reportError(req, err, 'A database update user exception will report to error db.', res);
                    else {
                        loginSuccess(res, member._id.toString(), member.access_token);
                    }
                })
            }
        });
}
/**
    Create an account from the fb account info.

*/
function createFBAccount(req, res) {
    let access_token = req.body.access_token;
    let user_id = req.body.user_id;
    //  graph API user basic profile request URL.
    //  profile image url will solve LARGE SIZE picture.
    let GRAPH_PROFILE = 'https://graph.facebook.com/' + user_id
                        + '?access_token=' + access_token
                        + '&fields=picture.type(large),name,id,gender,birthday';
    request.get(GRAPH_PROFILE, (err, response, body) => {
        if(err)
            reportError(req, err,
                'An FB Graph API error occured while getting user data in creating user account process.', res);
        if(response.statusCode != 200) {
            loginError(req, res, 'user_id or access_token is invalid. Check parameters.');
        }
        //  create member object
        //  save is complete, re-call loginFB() to perform login.
        let result = JSON.parse(body);
        let user = new Member({
            name: result.name,
            gender: result.gender,
            profile_img: result.picture.data.url,
            social: {
                facebook: {
                    id: result.id,
                    token: access_token
                }
            }
        });
        user.save((err) => {
            if(err)
                reportError(res, err, 'An account error while saving created user data.');
            loginFB(req, res);
        })
    });
}

/**
    Update user information except profile image.

    @api {POST} /user/:user_id/update Update user information except profile image
    @apiName updateUserInformation
    @apiGroup user

    @apiParam {String} access_token
    @apiParam {String} comment
    @apiParam {String} name
    @apiParam {String} place
    @apiParam {String} phone

    @apiSuccess {String} status It'll be ok.
*/
router.post('/user/:user_id/update', (req, res) => {
    var access_token = req.body.access_token;
    var user_id = req.params.user_id;
    var comment = req.body.comment;
    var name = req.body.name;
    var place = req.body.place;
    var phone = req.body.phone;

    //  Find user by receive user_id and inspect access_token.
    //  then save data
    Member.findOne({'_id': new mongoose.Types.ObjectId(user_id)})
        .exec(function(err, user) {
            if(!err && user) {
                Member.inspectAccessToken(access_token, (result) => {
                    // TODO change this when finishing upload sample data.
                    if(result.status) {
                        user.tattooist.comment = comment;
                        user.name = name;
                        user.tattooist.place = place;
                        user.phone = phone;
                        user.save((err) => {
                            if(!err) {
                                responser.success(res);
                            } else {
                                responser.error(res, 'Unable to save user info into db.', 500);
                                errorReporter.save(req.userIp, 'Failed to update user information.',
                                            'Unable to save user info into db.');
                            }
                        });
                    } else {
                        responser.error(res, 'Failed to inspect access_token.');
                        errorReporter.save(req.userIp, 'Failed to update user information.',
                                    'Failed to inspect access_token.');
                    }
                });
            } else if(!user){
                responser.error(res, 'Unable to find user by user_id.', 404);
                errorReporter.save(req.userIp, 'Failed to update user information.',
                'Unable to find user by user_id.');
            } else {
                responser.error(res, 'An exception occurred while querying member collection.', 500);
                    errorReporter.save(req.userIp, 'Failed to update user information.',
                                'An exception occurred while querying member collection.');
            }
        });
});

let profileUpload = multer({
    storage: multerS3({
        s3: s3upload.s3,
        bucket: 'dotattoo',
        acl: 'public-read',
        metadata: (req, file, cb) => {
            cb(null, { user_id: req.params.user_id });
        },
        key: (req, file, cb) => {
            let name = 'images/profile/' + 'profile.' + Date.now().toString();
            req.body.profileKey = name;
            cb(null, name);
        },
        contentType: multerS3.AUTO_CONTENT_TYPE
    })
});
/**
    @api {POST} /user/:user_id/update/profile
    @apiGroup user

    @apiParam {String} access_token
    @apiParam {File} photo
*/
router.post('/user/:user_id/update/image', profileUpload.single('photo'), (req, res) => {
    //  @deprecated
    //  TODO rename this ip address in real server.
    // const PROFILE_IMAGE_URL = 'http://52.79.95.175/profile/';

    var user_id = req.params.user_id;
    var access_token = req.body.access_token;
    var profileUrl = 'https://s3.ap-northeast-2.amazonaws.com/dotattoo/' + req.body.profileKey;

    Member.inspectAccessToken(access_token, (result) => {
        if(result.status === 'OK') {
            Member.findOne({ _id: new mongoose.Types.ObjectId(user_id) })
                .exec((err, user) => {
                    if(!err && user) {
                        user.profile_img = profileUrl;
                        user.save((err) => {
                            if(!err) {
                                responser.success(res, {
                                    profile_img: profileUrl
                                });
                            } else {
                                responser.error(res, 'Unable to save user profile image into db.', 500);
                            }
                        })
                    } else {
                        responser.error(res, 'Unable to find user to update profile image.', 404);
                    }
                });
        } else {
            responser.error(res, 'Failed to inspect access_tooen.', 401);
            errorReporter.save(req.userIp, 'Failed to update user image.', 'Failed to inspect access_tooen.');
        }
    });
});

/**
    Show top user list
    sort to best activities count.

    Not required access_token.
    Because of the not registered users.

    @api {GET} /users/top get top ranked 20 users
    @apiGroup user

    @apiSuccess {String} status It'll be 'OK'

*/
router.get('/users/top', (req, res) => {
    Post.aggregate([
        { $group: {
            _id: {
                'user_id': '$info.user_id'
            },
            user_count: { $sum: 1 }
        }},
        { $sort: {
            'user_count': -1
        }},
        { $project: {
            _id: '$_id.user_id',
            count: '$user_count'
        }},
        { $limit: 40 }
    ])
    .exec((err, users) => {
        if(!err && users) {
            let option = {
                path: '_id',
                model: 'Member'
            };
            Member.populate(users, option, (err, members) => {
                if(!err && members) {
                    let results = [];
                    members.forEach(function(member) {
                        results.push(member._id);
                    });
                    responser.success(res, results);
                } else {
                    //  population error
                    responser.error(res, 'An error occurred while getting top user in db.', 500);
                }
            })
        } else {
            responser.error(res, 'An error occurred while getting top user in db.', 500);
        }
    });
});

module.exports = router;
