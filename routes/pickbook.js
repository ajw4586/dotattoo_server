"use strict";
let express = require('express');
let router = express.Router();
let ObjectId = require('mongoose').Schema.Types.ObjectId;
let Member = require('../util/db-schema').Member;

router.route('/pickbook/:user_id')
    //  get pickbook list by user id.
    .get(function(req, res) {
        let user_id = req.params.user_id;
    })
    //  create pickbook to user.
    .post(function(req, res) {
        let access_token = req.body.access_token;
        let user_id = req.params.user_id;
    });

router.route('/pickbook/:user_id/:pickbook_id')
    //  get saved pickbook post.
    .get(function(req, res) {
        let user_id = req.params.user_id;
        let pickbook_id = req.params.pickbook_id;
    })
    //  save post into pickbook
    .post(function(req, res) {
        let user_id = req.params.user_id;
        let pickbook_id = req.params.pickbook_id;
        let access_token = req.body.access_token;
        let post_id = req.body.post_id;

        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                Pickbook.create();
            } else {
                //  Error response
            }
        });
    });


function success(res, data) {
    if(data) {
        res.json({
            status: 'OK',
            data: data
        });
    } else {
        res.json({
            status: 'OK',
            data: data
        });
    }
}
function error(res, message, statusCode) {
    if(statusCode) {
        res.status(statusCode).json({
            status: 'error',
            message: message
        });
    } else {
        res.status(401).json({
            status: 'error',
            message: message
        });
    }
}


module.exports = router;
