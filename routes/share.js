'use strict';
const express = require('express');
const router = express.Router();

router.route('/share/:type')
    .get((req, res) => {
        let type = req.query.type;
        let id = req.query.id;

        let option = {
            type: type,
            data: id
        };
        res.render('share', option);
    });

module.exports = router;
