'use strict';
const express = require('express');
const router = express.Router();

const SCHEMA_ROOT = require('../util/db-schema');
const Member = SCHEMA_ROOT.Member;
const Notification = SCHEMA_ROOT.Notification;
const responser = require('../util/responser');

router.route('/notification')
    /**
        @api {GET} /notification
        @apiName
    */
    .get((req, res) => {
        let access_token = req.query.access_token;

        Member.inspectAccessToken(access_token, (result) => {
            if(result.status === 'OK') {
                Notification.find()
                    .populate('user_id')
                    .populate('target_user_id')
                    .sort({ time: -1 })
                    .exec((err, notifications) => {
                        if(!err && notifications) {
                            responser.success(res, notifications);
                        } else {
                            responser.error(res, 'Notification not found.', 404);
                        }
                    });
            } else {
                //  error response
                responser.error(res, 'Invalid access_token.', 401);
            }
        });
    });

module.exports = router;
