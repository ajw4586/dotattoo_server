"use strict";
const express = require('express');
const mongoose = require('mongoose');
const util = require('util');
const fs = require('fs');
const multer = require('multer');
const multerS3 = require('multer-s3');
const router = express.Router();

const s3upload = require('../util/bucket');
const errorReporter = require('../util/error');
const responser = require('../util/responser');
const Post = require('../util/db-schema').Post;
const Member = require('../util/db-schema').Member;

let postUpload = multer({
    storage: multerS3({
        s3: s3upload.s3,
        bucket: 'dotattoo',
        acl: 'public-read',
        metadata: (req, file, cb) => {
            cb(null, { originalName: file.originalname });
        },
        key: (req, file, cb) => {
            let name = 'images/' + 'post.' + Date.now().toString();
            req.body.photoKey = name;
            cb(null, req.body.photoKey);
        },
        contentType: multerS3.AUTO_CONTENT_TYPE
    })
});

router.route('/post')
    /**
        @api {POST} /post upload a post
        @apiName uploadPost
        @apiDescription upload a tattoo post using multipart upload.
        @apiGroup post

        @apiParam {String} access_token
        @apiParam {File} photo
        @apiParam {String} comment
        @apiParam {String} part
        @apiParam {String} tattooist @deprecated
        @apiParam {String} title

        @apiSuccess {String} status

        @apiError {String }status
        @apiError {String} message

        @apiSampleRequest /post
    */
    .post(postUpload.single('photo'), (req, res) => {
        let access_token = req.body.access_token;
        let comment = req.body.comment;
        let part = req.body.part;
        let photoUrl = 'https://s3.ap-northeast-2.amazonaws.com/dotattoo/' + req.body.photoKey;
        let title = req.body.title;

        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                //  get uploaded photo's signed url synchronously.

                console.log('Saving post..');
                let post = new Post({
                    title: title,
                    comment: comment,
                    part: part,
                    photo_url: photoUrl,
                    info: {
                        user_id: result.user_id,
                        post_time: Date.now()
                    }
                });
                post.save(function(err) {
                    if(err) {
                        errorReporter.save(req.userIp, err, 'A database error occured while saving post data.');
                        responser.error(res, 'A database error occured while saving post data.', 500);
                    } else {
                        responser.success(res);
                    }
                });
            } else {
                errorReporter.save(req.userIp, result.message, 'An exception occured when upload post API requested.');
                responser.error(res, 'Failed to inspect access_token. Check your access_token.', 401);
            }
        });
    });

/**
    @deprecated
    create a new tattooist account
    from post data's tattooist name.
    callback when create account finish.
*/
function createTattooistAccount(tattooist_name, callback) {
    var tattooist = new Member({
        name: tattooist_name,
        type: 'tattooist'
    });
    tattooist.save(function(err, tattooist) {
        if(!err) {
            console.log('tattooist ' + tattooist);
            callback({
                status: 'OK',
                user: tattooist
            });
        } else {
            console.log('tattooist create error' + err);
            callback({
                status: 'ERROR'
            });
        }
    });
}

/**
    @api {GET} /post/:post_id get a post
    @apiName getPost
    @apiDescription get a post by given post_id
    @apiGroup post

    @apiParam {String} post_id
    @apiParam {String} [access_token]

    @apiSuccess {String} status
    @apiSuccess {String} data

    @apiError {String} status
    @apiError {String} message

    @apiSampleRequest /post/:post_id
*/
router.get('/post/:post_id', function(req, res) {
    let post_id = req.params.post_id;
    //  this param is optional.
    //  if you provide this, you would get liked field.
    let access_token = req.query.access_token;

    if(access_token) {
        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                getPostWithToken(result.user_id, req, res);
            } else {
                //  error response
                responser.error(res, 'access_token is not valid.', 401);
            }
        });
    } else {
        //  just query post by id
        getPostWithoutToken(req, res);
    }

    function getPostWithoutToken(req, res) {
        Post.findOne({ '_id': new mongoose.Types.ObjectId(post_id) })
                .populate('info.user_id', 'name profile_img tattooist.status')
                .populate('reaction.comment.commentBy', 'name profile_img')
                .exec(function(err, post) {
                    if(err) {
                        responser.error(res, 'DB error: ' + err, 500);
                    } else if(post) {
                        responser.success(res, post);
                    } else {
                        responser.error(res, 'Unable to find post.', 404);
                    }
        });
    }
    function getPostWithToken(user_id, req, res) {
        Post.aggregate([
            { $match: {
                '_id': new mongoose.Types.ObjectId(post_id)
            }},
            { $project: {
                //  put all post result's fields.
                title: 1,
                category: 1,
                comment: 1,
                part: 1,
                tattooist: 1,
                photo_url: 1,
                tag: 1,
                reaction: {
                    like: 1,
                    comment: 1,
                    comment_time: 1,
                    share: 1,
                    //  set boolean whether
                    //  the like array contains given token's user_id.
                    //
                    //  Use $setIntersection to find array has user_id
                    //  if the result size is over than 0,
                    //  $cond follows to return the given result.
                    liked: { $cond: [{
                        $gt: [{ $size: {
                            $setIntersection: ['$reaction.like', [user_id]]
                        }}, 0]
                    }, true, false]}
                },
                info: {
                    user_id: '$info.user_id',
                    post_time: '$info.post_time',
                    popularity: '$info.popularity'
                }
            }},
            { $sort: {
                'info.post_time': -1
            }}
        ], function(err, posts) {
            let options = [{
                path: 'info.user_id',
                model: 'Member',
                select: 'name profile_img tattooist.status'
            }, {
                path: 'reaction.comment.commentBy',
                model: 'Member',
                select: 'name profile_img'
            }];
            Post.populate(posts, options, function(err, posts) {
                if(!err && posts) {
                    responser.success(res, posts);
                } else {
                    responser.error(res, 'An error occurred while querying posts.', 404);
                }
            });
        });
    }
})


/**
    write a comment on posdts.
*/
router.route('/post/:id/comment')
    /**

        @api {POST} /post/:id/comment
        @apiName uploadComment
        @apiDescription upload a comment on post.
        @apiGroup post

        @apiParam {String} post_id
        @apiParam {String} access_token
        @apiParam {String} comment

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /post/:post_id/comment

    */
    .post(function(req, res) {
        let post_id = req.params.id;
        let access_token = req.body.access_token;
        let comment = req.body.comment;
        post_id = new mongoose.Types.ObjectId(post_id);
        if(!comment) {
            responser.error(res, 'Any comment cannot be found.', 400);
        }
        //  Check access token is valid.
        Member.inspectAccessToken(access_token, function(result) {
            //  Perform comment
            if(result.status == 'OK') {
                Member.getUserId(access_token, function(err, user) {
                    if(result.status == 'OK') {
                        if(!err && user) {
                            Post.comment(new mongoose.Types.ObjectId(post_id), user._id, comment, function(result) {
                                if(result.status == 'OK') {
                                    responser.success(res);
                                } else {
                                    responser.error(res, result.message, 404);
                                    errorReporter.save(req,userIp, 'Failed To comment.', 'An database exception while commenting on post process performing.');
                                }
                            });
                        } else {
                            responser.error(res, 'No user found by given access_token.', 404);
                            errorReporter.save(req,userIp, err, 'An database exception while commenting on post process performing.');
                        }
                    } else {
                        responser.error(res, 'Faild to get user_id. Check your access_token.', 401);
                        errorReporter.save(req,userIp, 'Failed to get user_id while processing comment on post.', 'An database exception while getting user_id by token.');
                    }
                });
            } else {
                responser.error(res, 'Failed to inspect access token. Check your access token.', 401);
                errorReporter.save(req,userIp, 'Failed to inspect access token.', 'An authentication failure while commenting on post process performing.');
            }
        });
    })
    //  you can call without access_token
    /**
        @api {GET} /post/:post_id/comment
        @apiName get comment on post
        @apiDescription get comments on a specific post by post_id
        @apiGroup post

        @apiParam {String} post_id

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} messageSchema

        @apiSampleRequest /post/:post_id/comment
    */
    .get(function(req, res) {
        let post_id = req.params.id;

        Post.findOne({ '_id': post_id })
        .select('reaction.comment')
        .populate('reaction.comment.commentBy', 'name profile_img')
        .exec(function(err, comments) {
            if(!err && comments) {
                responser.success(res, comments);
            } else {
                //  error response
                responser.error(res, 'Failed to get comment list.', 404);
            }
        });
    })
    /**
        @api {DELETE} /post/:post_id/comment_id delete a comment on post
        @apiName deleteComment
        @apiDescription delete a comment on post by comment_id.
        @apiGroup post

        @apiParam {String} comment_id

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /post/:post_id/comment
    */
    .delete(function(req, res) {
        let comment_id = req.params.id;
        let access_token = req.query.access_token;

        Post.find({ 'reaction.comment._id': comment_id })
        .exec(function(err, comment) {

        });
    });

/**
    like a post
*/
router.route('/post/:id/like')
    .post(function(req, res) {
        let post_id = req.params.id;
        let access_token = req.body.access_token;
        //  Check access token is valid.
        Member.inspectAccessToken(access_token, function(result) {
            //  like post
            if(result.status == 'OK') {
                Member.getUserId(access_token, function(err, user) {
                    if(!err && user) {
                        //  perform like
                        Post.like(new mongoose.Types.ObjectId(post_id), user._id, function(result) {
                            if(result.status == 'OK') {
                                responser.success(res);
                            } else {
                                responser.error(res, result.message, 400);
                            }
                        });
                    } else {
                        errorReporter.save(req.userIp, err, 'An exception occured while getting user id when processing like post.');
                        responser.error(res, 'Failed to get user_id. Check your access_token.', 401)
                    }
                });
            } else {
                errorReporter.save(req.userIp, 'Inspect access token failure.', 'An exception occurred while inspect access token when processing like post API.');
                responser.error(res, 'Failed to inspect access_token. Check your access_token.', 401);
            }
        })

    })
    .delete(function(req, res) {
        let access_token = req.query.access_token;
        let post_id = req.params.id;
        //  token inspection
        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                Post.unlike(new mongoose.Types.ObjectId(post_id), result.user_id, function(result) {
                    if(result.status == 'OK') {
                        //  Success response
                        responser.success(res);
                    } else {
                        //  Error response
                        responser.error(res, result.message, 400);
                    }
                });
            } else {
                //  Error response
                responser.error(res, 'invalid access_token.', 440300);
            }
        });
    });


router.get('/liked_post', function(req, res) {
    let access_token = req.query.access_token;
    Member.inspectAccessToken(access_token, function(result) {
        if(result.status == 'OK') {
            Post.find({ 'reaction.like': result.user_id })
            .populate('info.user_id')
            .exec(function(err, posts) {
                if(!err && posts) {
                    //  Success response
                    responser.success(res, posts);
                } else {
                    //  Error response
                    responser.error(res, 'Unable to find liked post.', 404);
                }
            });
        } else {
            //  Error response
            responser.error(res, 'Failed to inspect access token.');
        }
    });
});

/**
    create a filename using current time
    and given file name.
    Used to save photo into server.
    @return filename what is created by current time.
*/
function createFileName(filename) {
    return Date.now() + '-' + filename;
}

module.exports = router;
