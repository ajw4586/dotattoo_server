'use strict';
const SERVER_PUSH_ACCESS_KEY = 'AIzaSyDwYWjO9gsCE_UMIwghJtuAgl4TnakzM_g';
const SENDER_ID = '288557211023';

const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const gcm = require('node-gcm');

const Member = require('../util/db-schema').Member;
const PushToken = require('../util/db-schema').PushToken;
const WebPush = require('../util/db-schema').WebPush;

const responser = require('../util/responser');

function sendMessage(user, target_user, message, type) {
    let msg = new gcm.Message({
        collapseKey: 'test',
        delayWhileIdle: true,
        timeToLive: 3,
        data: {
            user_name: user.name,
            message: message,
            type: type
        }
    });
    PushToken.findOne({ user_id: target_user._id })
            .exec((err, regToken) => {
                if(!err && regToken) {
                    let sender = new gcm.Sender(SERVER_PUSH_ACCESS_KEY);
                    //  perform gcm push when registration_id array is available.
                    sender.send(msg, { registrationTokens: regToken.registration_ids }, function(err, result) {
                        if(err) {
                            console.log('gcm push failure.');
                        }
                    });
                } else {
                    console.log('gcm failure.');
                    return;
                }
            });
}

router.route('/push/register')
    /**
        @api {POST} /push/register save gcm registration_id
        @apiName uploadRegistrationToken
        @apiGroup push

        @apiParam {String} access_token
        @apiParam {String} registration_id

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /push/register
    */
    .post(function(req, res) {
        let access_token = req.body.access_token;
        let registration_id = req.body.registration_id;

        //  inspect access_token
        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                PushToken.addId(registration_id, result.user_id, function(result) {
                    if(result.status == 'OK') {
                        success(res);
                    } else {
                        error(res, 'Failed to record registration_id.', 400);
                    }
                });
            } else {
                //  error response
                error(res, 'Failed to inspect access_token. Check your access_token!');
            }
        });

    })
    /**
        @api {DELETE} /push/register delete gcm registration_id
        @apiName deletePushToken
        @apiDescription delete GCM push registration_id from database.
        @apiGroup push

        @apiParam {String} access_token
        @apiParam {String} registration_id

        @apiSuccess {String} status

        @apiError {String} status
        @apiError {String} message

        @apiSampleRequest /push/register
    */
    .delete(function(req, res) {
        let access_token = req.query.access_token;
        let registration_id = req.query.registration_id;

        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                PushToken.removeId(registration_id, new mongoose.Types.ObjectId(result.user_id),
                function(result) {
                    if(result.status == 'OK') {
                        success(res);
                    } else {
                        error(res, 'Failed to record registration_id.', 400);
                    }
                });
            } else {
                //  error response
                error(res, 'Failed to inspect access_token. Check your access_token!');
            }
        });
    });

/**
    @api {POST} /push/console_send send push message from manage console
    @apiName consolePushAPI
    @apiGroup push

    @apiParam {String} access_token console access_token
    @apiParam {String} push_message what you want to show your users.
    @apiParam {String} img_link popup image url
    @apiParam {String} user_ids user_id array that you want to send push.

    @apiSuccess {String} status

    @apiError {String} status
    @apiError {String} message

    @apiSampleRequest /push/console_send

*/
router.route('/push/console_send')
    .post((req, res) => {
        const access_token = req.body.access_token;
        let push_message = req.body.push_message;
        let push_message_content = req.body.push_message_content;
        let img_link = req.body.img_link;
        let registration_ids = req.body.registration_ids;
        let target_user_ids = req.body.target_user_ids;

        //  TODO: determine img_link handle
        let msg;

        let sender = new gcm.Sender(SERVER_PUSH_ACCESS_KEY);
        //  TEST code for debug push.
        if(process.env.MODE === 'publish') {
            msg = new gcm.Message({
                collapseKey: 'dotattoo_ad',
                restricted_package_name: 'com.dotattoo.android',
                data: {
                    message: push_message,
                    message_content: push_message_content,
                    //  type is only allowed 'ad', 'message'
                    type: 'ad'
                }
            });
        } else {
            msg = new gcm.Message({
                collapseKey: 'dotattoo_ad',
                restricted_package_name: 'com.dotattoo.android',
                dryRun: true,
                data: {
                    message: push_message,
                    message_content: push_message_content,
                    //  type is only allowed 'ad', 'message'
                    type: 'ad'
                }
            });
        }
        //  perform gcm push when registration_id array is available.
        sender.send(msg, { registrationTokens: registration_ids }, (err, result) => {
            if(err) {
                console.log('gcm push failed. ' + err);
                responser.error(res, 'gcm push failed. ' + err);
            } else {
                //  send success response and save push message sent history
                let ids = [];
                target_user_ids.forEach((id) => {
                    ids.push(new mongoose.Types.ObjectId(id));
                });
                new WebPush({
                    message: push_message,
                    message_content: push_message_content,
                    img_url: img_link,
                    target_user_ids: ids,
                    send_time: Date.now()
                }).save((err) => {
                    if(err) {
                        console.log('An error occured while saving push data into histroy. ' + err);
                    }
                    responser.success(res);
                });
            }
        });
    });


function success(res) {
    res.json({
        status: 'OK'
    });
}
function error(res, message, statusCode) {
    if(statusCode) {
        res.status(statusCode).json({
            status: 'error',
            message: message
        });
    } else {
        res.status(401).json({
            status: 'error',
            message: message
        });
    }
}

module.exports = {
    push: sendMessage,
    router: router
};
