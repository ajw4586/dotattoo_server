var express = require('express');
var router = express.Router();
var errorReporter = require('../util/error');

var Error = require('../util/db-schema').Error;
var Posts = require('../util/db-schema').Post;
var Member = require('../util/db-schema').Member;

/**
    @api {GET} /timeline get DOTATTOO timeline
    @apiName getTimeline
    @apiGroup timeline

    @apiParam {String} [access_token] if it is provided, timeline information with liked information will be served.
    @apiSampleRequest /timeline

    @apiSuccess {String} status
    @apiSuccess {JSONArray} data

    @apiError {String} status
    @apiError {String} message
*/
router.get('/timeline', function(req, res) {
    var access_token = req.query.access_token;
    //  Verify access token
    //  if access token is null, just show recent timeline
    if(access_token) {
        showTimeline(req, res, access_token);
    } else {
        showTimelineWithoutInfo(req, res);
    }


    function showTimeline(req, res, access_token) {
        Member.inspectAccessToken(access_token, function(result) {
            if(result.status == 'OK') {
                //   able to get result that contains user liked specific post.
                console.log('user_id: ' + result.user_id)
                Posts.aggregate([
                    { $project: {
                        //  put all post result's fields.
                        title: 1,
                        category: 1,
                        comment: 1,
                        part: 1,
                        tattooist: 1,
                        photo_url: 1,
                        tag: 1,
                        reaction: {
                            like: 1,
                            comment: 1,
                            comment_time: 1,
                            share: 1,
                            //  set boolean whether
                            //  the like array contains given token's user_id.
                            //
                            //  Use $setIntersection to find array has user_id
                            //  if the result size is over than 0,
                            //  $cond follows to return the given result.
                            liked: { $cond: [{
                                $gt: [{ $size: {
                                    $setIntersection: ['$reaction.like', [result.user_id]]
                                }}, 0]
                            }, true, false]}
                        },
                        info: {
                            user_id: '$info.user_id',
                            post_time: '$info.post_time',
                            popularity: '$info.popularity'
                        }
                    }},
                    { $sort: {
                        'info.post_time': -1
                    }}
                ], function(err, posts) {
                    var options = [{
                        path: 'info.user_id',
                        model: 'Member'
                    }];
                    //  populate user_id field && reaction field.
                    Posts.populate(posts, options, function(err, posts) {
                        if(!err && posts) {
                            res.json({
                                status: 'OK',
                                data: posts
                            });
                        } else {
                            errorResponse(req, res, 404, 'An error occured when querying posts. Err: ' + err);
                        }
                    });
                });
            } else if(result.status == 'error') {
                console.log('access_token inspection failed. Show timeline instead of aggregation.');
                showTimelineWithoutInfo(req, res);
            }
        });
    }
    function showTimelineWithoutInfo(req, res) {
        Posts.find().sort({ 'info.popularity': 'desc', 'info.post_time': 'desc' })
        .populate('reaction.like')
        .populate('info.user_id')
        .populate('reaction.comment.commentBy')
        .exec(function(err, posts) {
            if(!err && posts) {
                res.json({
                    status: 'OK',
                    data: posts
                });
            } else {
                errorResponse(req, res, 404, 'An error occured when querying posts. Err: ' + err);
            }
        });
    }

    var errorResponse = function(req, res, statusCode, message) {
        errorReporter.save(req.userIp, 'An exception occured when getting timeline API requested.', message);
        res.status(statusCode).json({
            status: 'ERROR',
            message: message
        });
    };
});

module.exports = router;
