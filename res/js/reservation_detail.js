'use strict';
const reservation_detail = $("#res_detail_modal");
const input_file = $('#reservation-file');
const img_prev = $('#img_prev');
const confirm = $('#reservation_confirm');

reservation_detail.on('show.bs.modal', (event) => {
    //  clear image preview src
    img_prev.attr('src', '#');
    $('.btn-warning.btn-file').removeClass('btn-warning').addClass('btn-info').tooltip('hide');

    const reservation = $(event.relatedTarget);
    const info = reservation.data('reservation');
    if (info) {
        //  set reservation information to modal
        $('#res_detail_modal_title').text(info.request.user_id.name + '님의 타투 예약정보');
        $('#res_detail_modal_tattooist_name').text(info.request.tattooist_id.name);
        $('#res_detail_modal_tattoo_img').attr('src', info.request.info.photo);
        $('#res_detail_modal_tattoo_name').text(info.request.info.tattoo_name);
        $('#res_detail_modal_tattoo_genre').text(info.request.info.genre);
        $('#res_detail_modal_tattoo_part').text(info.request.info.part);
        $('#res_detail_modal_tattoo_place').text(info.request.info.place);
        //  parse Date
        let res_date = new Date(info.request.info.date);
        $('#res_detail_modal_reservation_date').text(res_date.getFullYear() + '년 ' + (res_date.getMonth()+1) + '월 ' + res_date.getDate() + '일 ' + res_date.getHours() + '시 ' + res_date.getMinutes() + '분');
        $('#res_detail_modal_total_price').text(info.request.info.price.toLocaleString() + '원');
        $('#res_detail_modal_reservation_price').text(info.request.info.reservation_price.toLocaleString() + '원');
        //  parse Time
        let res_time = new Date(info.reservation_time);
        $('#res_detail_modal_reservation_time').text(res_time.getFullYear() + '년 ' + (res_time.getMonth()+1) + '월 ' + res_time.getDate() + '일 ' + res_time.getHours()+ + '시 ' + res_time.getMinutes() + '분 ' + res_time.getSeconds() + '초');
    } else {

    }
});

input_file.change(() => {
    readPrevImg((input_file)[0]);
});

function readPrevImg(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            img_prev.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        console.error('Can\'t load preview image.');
    }
}

confirm.on('click', (ev) => {
    //  if image is not selected, do not confirm reservation.
    if(!img_prev.attr('src') === 'undefined') {

    } else {
        $('.btn-file').removeClass('btn-info').addClass('btn-warning').tooltip('show');
    }
});
