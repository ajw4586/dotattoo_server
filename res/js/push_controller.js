'use strict';
const targetUserIdsTable = $('#target_user_ids_table');
const userToPushCheckAll = $('#push_check_all');
const push_send = $('#push_send_btn');
const push_message_field = $('#push_message_field');
const push_message_content_field = $('#push_message_content_field');
const alert_container = $('#alert_container');
const push_confirm_modal = $('#push_confirm_modal');
const push_confirm_message_title = $('#push_confirm_message_title');
const push_confirm_message_content = $('#push_confirm_message_content');
const push_confirm_btn = $('#push_confirm');

push_send.on('click', (ev) => {
    ev.preventDefault();
    //  input data check
    if(push_message_field.val() === '' || push_message_content_field.val() === '') {
        makeAlert(alert_container, '푸시 메세지 내용을 입력해주세요.', { type: 'warning', sec: 3 });
        return;
    }

    //  get selected device push token from table
    const tableData = targetUserIdsTable.bootstrapTable('getAllSelections');
    let resultIds = [];
    let userIds = [];

    tableData.forEach((ids) => {
        resultIds = resultIds.concat(ids.token.split('\ntoken.'));
        userIds.push(ids.user_id);
    });
    if(resultIds.length <= 0) {
        makeAlert(alert_container, '광고를 보낼 유저를 선택해주세요. (최소 1명/최대 1000명)', { type: 'warning', sec: 3 });
        return;
    }

    push_confirm_modal.data('result_ids', resultIds);
    push_confirm_modal.data('user_ids', userIds);
    push_confirm_modal.modal();
});

push_confirm_btn.on('click', (ev) => {
    push_confirm_modal.modal('hide');

    const resultIds = push_confirm_modal.data('result_ids');
    const userIds = push_confirm_modal.data('user_ids');

    $.ajax({
        url: '/push/console_send',
        type: 'POST',
        contentType: 'application/json',
        dataTyoe: 'json',
        data: JSON.stringify({
            access_token: $.cookie('access_token'),
            push_message: push_message_field.val(),
            push_message_content: push_message_content_field.val(),
            registration_ids: resultIds,
            target_user_ids: userIds
        }),
        success: (data) => {
            if(data && data.status == 'OK') {
                //  success handle
                makeAlert(alert_container, '푸시알림 요청을 하였습니다.', { type: 'success', sec: 7 })
            } else {
                console.log('push notification request failure.');
                makeAlert(alert_container, '푸시알림 요청에 실패했습니다.', { type: 'danger', sec: 7 });
            }
        },
        error: () => {
            console.log('push notification request failure.');
            makeAlert(alert_container, '푸시알림 요청에 실패했습니다.', { type: 'danger', sec: 7 });
        }
    });
});

push_confirm_modal.on('show.bs.modal', () => {
    push_confirm_message_title.text(push_message_field.val());
    push_confirm_message_content.text(push_message_content_field.val());
});
