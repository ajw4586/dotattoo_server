'use strict';
$('#login_btn').on('click', (ev) => {
    let id = $('#id_field').val();
    let pw = $('#pw_field').val();

    // console.log('login requested. id is ' id + ' pw is ' + pw);
    $.ajax({
        url: '/console/login',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
            id: id,
            pw: pw
        }),
        success: (data) => {
            if(data && data.status == 'OK') {
                //  set cookie 'access_token'
                $.cookie('access_token', data.data.access_token)
                const CONSOLE_PAGE = 'http://admin.dotattoo.kr/console';
                window.location.replace(CONSOLE_PAGE);
            } else {
                alert('로그인에 실패하였습니다.');
            }
        },
        error: () => {
            alert('로그인에 실패하였습니다.');
        }
    });
});
