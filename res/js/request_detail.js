'use strict';
const request_detail_modal = $('#tattooist_request_detail_modal');
const delete_modal = $('#delete_modal');
const delete_modal_btn = $('#delete_modal_btn');
const tattooist_request_submit_btn = $('#tattooist_request_submit');
const tattooist_request_delete_btn = $('#tattooist_request_delete');
const alert_placeholder = $('#tattooist_request_alert');
const delete_request_confirm_btn = $('#delete_request_confirm');

request_detail_modal.on('show.bs.modal', (ev) => {
    const request = $(ev.relatedTarget);
    let request_info = request.data('request');

    if(request_info) {
        $('#request_header').text(request_info.real_name + '님의 타투이스트 신청정보')
        $('#real_name').text(request_info.real_name);
        $('#tattooist_name').text(request_info.request_user_id.name);
        $('#place').text(request_info.place);
        $('#address').text(request_info.address);
        $('#email').text(request_info.email);
        $('#phone').text(request_info.phone);
        //  parse time
        let req_time = new Date(request_info.requested_time);
        $('#requested_time').text(req_time.getFullYear() + '년 ' + req_time.getMonth()+1 + '월 ' + req_time.getDay() + '일 ' + req_time.getHours() + '시 ' + req_time.getMinutes() + '분 ' + req_time.getSeconds() + '초');
        $('#introduction').text(request_info.introduction);

        //  set information to delete button
        tattooist_request_submit_btn.data('request', request_info);
        delete_modal.data('request', request_info);
        if(request_info.status == 'done') {
            request_detail_modal.find('.modal-footer').addClass('hidden');
        } else {
            request_detail_modal.find('.modal-footer').removeClass('hidden');
        }
    }
});

delete_modal.on('shown.bs.modal', (ev) => {
    const request = $(ev.relatedTarget);
    let request_info = delete_modal.data('request');

    if(request_info) {
        $('#delete_tattooist_request_user_name').text(request_info.real_name + '님의 타투이스트 신청을 삭제하시겠습니까?')
        delete_modal.find('#real_name').text(request_info.real_name);
        delete_modal.find('#tattooist_name').text(request_info.request_user_id.name);
        delete_modal.find('#place').text(request_info.place);
        delete_modal.find('#address').text(request_info.address);
        delete_modal.find('#email').text(request_info.email);
        delete_modal.find('#phone').text(request_info.phone);
        //  parse time
        let req_time = new Date(request_info.requested_time);
        delete_modal.find('#requested_time').text(req_time.getFullYear() + '년 ' + req_time.getMonth()+1 + '월 ' + req_time.getDay() + '일 ' + req_time.getHours() + '시 ' + req_time.getMinutes() + '분 ' + req_time.getSeconds() + '초');
        delete_modal.find('#introduction').text(request_info.introduction);
    }
});

delete_modal_btn.on('click', (ev) => {
    ev.preventDefault();
    ev.stopPropagation();
    let request_info = delete_modal_btn.data('request');
    if(request_info) {
        delete_modal.data('request', request_info);
    }
    //  show delete modal
    delete_modal.modal();
});

tattooist_request_submit_btn.on('click', () => {
    console.log('submit btn clicked.');
    request_detail_modal.modal('hide');
    confirm_tattooist_request(tattooist_request_submit_btn.data('request'));
});

tattooist_request_delete_btn.on('click', () => {
    request_detail_modal.modal('hide')
    delete_modal.modal();
});

delete_request_confirm_btn.on('click', () => {
    console.log('delete request clicked.');
    const req_info = delete_modal.data('request');
    delete_modal.modal('hide');
    confirm_delete_request(req_info);
});

function confirm_delete_request(req_info) {
    $.ajax({
        url: '/tattoo/tattooist/request/delete',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            access_token: $.cookie('access_token'),
            request_id: req_info._id
        }),
        dataType: 'json',
        success: (data) => {
            let popupAlert = (id) => {
                setTimeout(() => {
                    alert_placeholder.find('#' + id).alert('close');
                }, 10000);
            }

            const id = Date.now();
            alert_placeholder.html('<div id="' + id + '" class="overlay alert alert-success fade in" role="alert">' +
                                '<button data-dismiss="alert" class="close" role="button" >&times;</button>' +
                                '<span>타투이스트 신청을 삭제하였습니다.</span>' +
                        '</div>');
            popupAlert(id);
        },
        error: () => {
            let popupAlert = (id) => {
                setTimeout(() => {
                    alert_placeholder.find('#' + id).alert('close');
                }, 10000);
            }

            const id = Date.now();
            alert_placeholder.html('<div id="' + id + '" class="overlay alert alert-danger fade in" role="alert">' +
                                '<button data-dismiss="alert" class="close" role="button" >&times;</button>' +
                                '<span>타투이스트 신청을 삭제하는 동안 문제가 생겼습니다.</span>' +
                        '</div>');
            popupAlert(id);
        }
    });
}

function confirm_tattooist_request(req_info) {
    var errorAlert = () => {
        let popupAlert = (id) => {
            setTimeout(() => {
                alert_placeholder.find('#' + id).alert('close');
            }, 10000);
        }

        const id = Date.now();
        alert_placeholder.html('<div id="' + id + '" class="overlay alert alert-danger fade in" role="alert">' +
                            '<button data-dismiss="alert" class="close" role="button" >&times;</button>' +
                            '<span>타투이스트 신청을 처리하는 동안 문제가 생겼습니다.</span>' +
                    '</div>');
        popupAlert(id);
    };

    $.ajax({
        url: '/tattoo/tattooist/register',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            access_token: $.cookie('access_token'),
            request_id: req_info._id,
            request_user_id: req_info.request_user_id._id,
            real_name: req_info.real_name,
            place: req_info.place,
            address: req_info.address,
            email: req_info.email,
            phone: req_info.phone,
            introduction: req_info.introduction
        }),
        dataType: 'json',
        success: (data) => {

            if(data.status && data.status == 'OK') {
                let popupAlert = (id) => {
                    setTimeout(() => {
                        alert_placeholder.find('#' + id).alert('close');
                    }, 10000);
                }

                const id = Date.now();
                alert_placeholder.html('<div id="' + id + '" class="overlay alert alert-success fade in" role="alert">' +
                                    '<button data-dismiss="alert" class="close" role="button" >&times;</button>' +
                                    '<span>타투이스트 신청이 처리되었습니다.</span>' +
                            '</div>');
                popupAlert(id);
            } else {
                errorAlert();
            }
        },
        error: () => {
            errorAlert();
        }
    });
}
