'use stict';
const alert_container = $('.alert_container');
const change_contact = $('.state-contact');
const change_consult = $('.state-consult');
const info_modal = $('#tattooist_info_modal');

change_contact.on('click', (ev) => {
    ev.stopPropagation();

    const clicked = $(ev.target);
    console.log(clicked.data('tattooist'));
    let tattooist_data = clicked.data('tattooist');
    clicked.button('변경 중');

    if(tattooist_data) {
        $.ajax({
            url: '/console/tattooist/state_change',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                access_token: $.cookie('access_token'),
                target_user_id: tattooist_data._id,
                type: 'contact'
            }),
            dataType: 'json',
            success: (data) => {
                if(data && data.status === 'OK') {
                    //  if it changed to true,
                    //  change button state to success
                    if(data.data.state) {
                        clicked.removeClass('btn-default').addClass('btn-success');
                    } else {
                        clicked.removeClass('btn-success').addClass('btn-default');
                    }
                    //  alert the result
                    makeAlert(alert_container, '정보가 변경되었습니다.', { type: 'success' });
                } else {
                    //  error handling
                    makeAlert(alert_container, '정보를 변경하는데 실패했습니다.', { type: 'danger' });
                }
            },
            error: () => {
                makeAlert(alert_container, '정보를 변경하는데 실패했습니다.', { type: 'danger' });
            }
        });
    }
});

change_consult.on('click', (ev) => {
    ev.stopPropagation();
    const clicked = $(ev.target);
    let tattooist_data = clicked.data('tattooist');
    clicked.button('변경 중');

    if(tattooist_data) {
        $.ajax({
            url: '/console/tattooist/state_change',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                access_token: $.cookie('access_token'),
                target_user_id: tattooist_data._id,
                type: 'consult'
            }),
            dataType: 'json',
            success: (data) => {
                if(data && data.status === 'OK') {
                    //  if it changed to true,
                    //  change button state to success
                    if(data.data.state) {
                        clicked.removeClass('btn-default').addClass('btn-success');
                    } else {
                        clicked.removeClass('btn-success').addClass('btn-default');
                    }
                    //  alert the result
                    makeAlert(alert_container, '정보가 변경되었습니다.', { type: 'success' });
                } else {
                    //  error handling
                    makeAlert(alert_container, '정보를 변경하는데 실패했습니다.', { type: 'danger' });
                }
            },
            error: () => {
                makeAlert(alert_container, '정보를 변경하는데 실패했습니다.', { type: 'danger' });
            }
        });
    }
});

info_modal.on('show.bs.modal', (ev) => {
    const tattooist_data = $(ev.relatedTarget).data('tattooist');

    //  put data each field
    if(tattooist_data) {
        $('#tattooist_user_info_header_title').text(tattooist_data.name + '님의 정보');
        $('#tattooist_info_profile_img').attr('src', tattooist_data.profile_img || '#');
        $('#tattooist_info_real_name').text(tattooist_data.tattooist.name);
        $('#tattooist_info_name').text(tattooist_data.name);
        $('#tattooist_info_address').text(tattooist_data.tattooi);
        $('#tattooist_info_place').text(tattooist_data.tattooist.place);
        $('#tattooist_info_real_address').text(tattooist_data.tattooist.address);
        $('#tattooist_info_phone').text(tattooist_data.phone);
        $('#tattooist_info_email').text(tattooist_data.email);
        $('#ttattooist_info_account')
            .text(!tattooist_data.bank ? '-' : tattooist_data.bank_account.bank + ' '
                + tattooist_data.bank_account.account_num + ' '
                + tattooist_data.bank_account.owner);
        if(tattooist_data.social) {
            $('#tattooist_info_account_type')
                .text(tattooist_data.social.facebook ? '페이스북' : (tattooist_data.social.kakao ? '카카오' : '기타'));
        } else {
            $('#tattooist_info_account_type').text('-');
        }
        $('#tattooist_info_comment').text(tattooist_data.tattooist_comment);

    } else {
        console.log('tattooist info undefined');
    }

});
