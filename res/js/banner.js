'use strict';
const add_banner_modal = $('#add_banner_modal');
const banner_image = $('#banner_image');
const banner_image_preview = $('#banner_image_preview');
const banner_detail = $('#banner_detail');
const banner_detail_preview = $('#banner_detail_preview');
const banner_description = $('#banner_description');
const banner_image_btn = $('#banner_image_btn');
const banner_detail_btn = $('#banner_detail_btn');
const submit = $('#banner_submit');
const alert_container = $('#alert_container');

//  clear previous preview images
add_banner_modal.on('show.bs.modal', (ev) => {
    banner_image_preview.attr('src', '#');
    banner_detail_preview.attr('src', '#');
    banner_image_btn.removeClass('btn-warning').addClass('btn-info');
    banner_detail_btn.removeClass('btn-warning').addClass('btn-info');
});

banner_image_btn.change(() => {
    readPrevImg((banner_image)[0], banner_image_preview);
});

banner_detail_btn.change(() => {
    readPrevImg((banner_detail)[0], banner_detail_preview);
});

function readPrevImg(input, preview) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            preview.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        console.error('Can\'t load preview image.');
    }
}

submit.on('click', (ev) => {
    //  check image is b
    if(banner_image_preview.attr('src') === '#' || banner_detail_preview.attr('src') === '#') {
        banner_image_btn.removeClass('btn-info').addClass('btn-warning');
        banner_detail_btn.removeClass('btn-info').addClass('btn-warning');
        return;
    }
    //  create form data object
    let banner_data = new FormData();
    banner_data.append('access_token', $.cookie('access_token'));
    banner_data.append('description', banner_description.val());
    banner_data.append('banner_img', banner_image[0].files[0]);
    banner_data.append('detail_img', banner_detail[0].files[0]);


    //  submit banner information to server
    $.ajax({
        url: '/console/banner',
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: banner_data,
        processData: false,
        contentType: false,
        success: (data) => {
            if(data && data.status === 'OK') {
                makeAlert(alert_container, '배너를 등록했습니다.', { type: 'success', sec: 5 })
                //  close modal and reload page
                add_banner_modal.modal('hide');
                $('body').load(location.href + '#banner_table');
            } else {
                //  error handling
                makeAlert(alert_container, '배너를 등록하지 못했습니다.', { type: 'danger', sec: 5 });
            }
        },
        error: () => {
            //  error handling
            makeAlert(alert_container, '배너를 등록하지 못했습니다.', { type: 'danger', sec: 5 });
        }
    });
});
