'use strict';

/*
    JQuery required.
    Show temporary showing alert box
    It will be disappeared after your time millis.(default: 2 sec)

    options have 'type', 'sec' fields.
*/
function makeAlert(container, message, options) {
    if(!container) {
        return;
    }
    if(!options) {
        options = {};
    }
    if(!options.type) {
        options.type = 'info';
    }
    if(!options.sec) {
        options.sec = 2;
    }
    console.log('alert building requested.');

    const time_based_id = Date.now();
    container.append($('<div/>', {
        id: time_based_id,
        class: 'alert alert-' + options.type + ' overlay fade in',
        role: 'alert',
        text: message
    }));
    setTimeout(() => {
        container.find('#' + time_based_id).alert('close');
    }, options.sec * 1000);
}
